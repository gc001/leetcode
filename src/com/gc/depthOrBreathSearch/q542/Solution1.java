package com.gc.depthOrBreathSearch.q542;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-15 15:01
 */
class Solution1 {
    public int[][] updateMatrix(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0)
            return new int[][]{};
        int n1 = matrix.length;
        int n2 = matrix[0].length;
        int[][] arr = new int[n1][n2];
        Queue<int[]> q = new LinkedList<>();
        for (int i = 0; i < n1; i++)
            for (int j = 0; j < n2; j++) {
                if (matrix[i][j] == 0) {
                    arr[i][j] = 0;
                    q.offer(new int[]{i, j});
                } else {
                    arr[i][j] = Integer.MAX_VALUE;
                }
            }

        int[][] cors = {{0, -1}, {-1, 0}, {0, 1}, {1, 0}};
        while (q.size() > 0) {
            int[] cor = q.poll();
            int row = cor[0];
            int col = cor[1];
            for (int j = 0; j < 4; j++) {
                int x = cors[j][0] + row;
                int y = cors[j][1] + col;
                if (x >= 0 && x < n1 && y >= 0 && y < n2 && matrix[x][y] == 1) {
                    arr[x][y] = Math.min(arr[x][y], 1 + arr[row][col]);
                    matrix[x][y] = 0;
                    q.offer(new int[]{x, y});
                }
            }

        }
        return arr;
    }

}
