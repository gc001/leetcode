package com.gc.depthOrBreathSearch.q542;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 17:25
 */
public class Test {
    public int[][] updateMatrix(int[][] mat) {
        int m = mat.length;
        int n = mat[0].length;
        Queue<int[]> q = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (mat[i][j] == 0) {
                    q.offer(new int[]{i, j});
                } else {
                    mat[i][j] = Integer.MAX_VALUE;
                }
            }
        }
        int[][] dirs = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        while (!q.isEmpty()) {
            int[] poll = q.poll();
            for (int[] dir : dirs) {
                int x = poll[0] + dir[0];
                int y = poll[1] + dir[1];
                if (x >= 0 && x < m && y >= 0 && y < n && mat[x][y] > 1 + mat[poll[0]][poll[1]]) {
                    mat[x][y] = 1 + mat[poll[0]][poll[1]];
                    q.offer(new int[]{x, y});
                }
            }
        }
        return mat;
    }
}