package com.gc.depthOrBreathSearch.q542;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description: 01 矩阵
 * @author: gc
 * @create: 2022-05-15 14:32
 */
public class Solution {
    public int[][] updateMatrix(int[][] mat) {
        int rows = mat.length;
        int cols = mat[0].length;
        Deque<int[]> q = new LinkedList<>();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (mat[i][j] == 0) {
                    q.offer(new int[]{i, j});
                } else {
                    mat[i][j] = Integer.MAX_VALUE;
                }
            }
        }

        int[][] dirs = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};

        while (!q.isEmpty()) {
            int[] coordinate = q.pop();
            for (int[] dir : dirs) {
                int x = dir[0] + coordinate[0];
                int y = dir[1] + coordinate[1];
                if (x < 0 || x >= rows || y < 0 || y >= cols || mat[x][y] <= 1 + mat[coordinate[0]][coordinate[1]])
                    continue;
                mat[x][y] = 1 + mat[coordinate[0]][coordinate[1]];
                q.offer(new int[]{x, y});
            }
        }
        return mat;
    }
}