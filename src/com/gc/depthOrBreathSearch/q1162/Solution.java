package com.gc.depthOrBreathSearch.q1162;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description: 地图分析
 * @author: gc
 * @create: 2022-05-15 16:23
 * <p>
 * 相信对于Tree的BFS大家都已经轻车熟路了：要把root节点先入队，然后再一层一层的无脑遍历就行了。
 * <p>
 * 对于图的BFS也是一样滴～ 与Tree的BFS区别如下：
 * 1、tree只有1个root，而图可以有多个源点，所以首先需要把多个源点都入队。
 * 2、tree是有向的因此不需要标志是否访问过，而对于无向图来说，必须得标志是否访问过！
 * 并且为了防止某个节点多次入队，需要在入队之前就将其设置成已访问！
 * <p>
 * 这是一道典型的BFS基础应用，为什么这么说呢？
 * 因为我们只要先把所有的陆地都入队，然后从各个陆地同时开始一层一层的向海洋扩散，那么最后扩散到的海洋就是最远的海洋！
 * 并且这个海洋肯定是被离他最近的陆地给扩散到的！
 * <p>
 * 作者：sweetiee
 * 链接：https://leetcode.cn/problems/as-far-from-land-as-possible/solution/jian-dan-java-miao-dong-tu-de-bfs-by-sweetiee/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Solution {
    // 陆地感染海洋
    public int maxDistance(int[][] grid) {
        int rows = grid.length;
        int cols = grid[0].length;
        Deque<int[]> q = new LinkedList<>();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == 1) q.offer(new int[]{i, j});
            }
        }
        if (q.isEmpty() || q.size() == rows * cols) return -1;
        int[][] xy = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
        int res = 0;
        while (!q.isEmpty()) {
            res++;
            int size = q.size();
            // 每一批入队的陆地都同一批次去感染海洋
            for (int i = 0; i < size; i++) {
                int[] poll = q.poll();
                for (int[] dir : xy) {
                    int x = dir[0] + poll[0];
                    int y = dir[1] + poll[1];
                    if (x < rows && x >= 0 && y < cols && y >= 0 && grid[x][y] == 0) {
                        grid[x][y] = 1;
                        q.offer(new int[]{x, y});
                    }
                }

            }
        }
        // 第一步是自身，需要减掉
        return res - 1;
    }
}