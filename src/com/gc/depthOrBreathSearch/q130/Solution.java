package com.gc.depthOrBreathSearch.q130;

/**
 * @program: leetcode
 * @description: 被围绕的区域
 * @author: gc
 * @create: 2022-05-15 11:53
 */
public class Solution {

    public void solve(char[][] board) {
        int rows = board.length;
        int cols = board[0].length;
        for (int i = 0; i < rows; i++) {
            helper(board, i, 0, rows, cols);
            helper(board, i, cols - 1, rows, cols);
        }
        for (int j = 0; j < cols; j++) {
            helper(board, 0, j, rows, cols);
            helper(board, rows - 1, j, rows, cols);
        }
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (board[i][j] == '-') board[i][j] = 'O';
                else if (board[i][j] == 'O') board[i][j] = 'X';
            }
        }
    }


    void helper(char[][] board, int i, int j, int rows, int cols) {
        if (i >= rows || i < 0 || j >= cols || j < 0 || board[i][j] == 'X' || board[i][j] == '-') return;
        board[i][j] = '-';
        helper(board, i - 1, j, rows, cols);
        helper(board, i + 1, j, rows, cols);
        helper(board, i, j - 1, rows, cols);
        helper(board, i, j + 1, rows, cols);
    }
}