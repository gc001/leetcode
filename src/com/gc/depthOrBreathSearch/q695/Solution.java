package com.gc.depthOrBreathSearch.q695;

/**
 * @program: leetcode
 * @description: 岛屿的最大面积
 * @author: gc
 * @create: 2022-05-15 13:25
 */
public class Solution {

    public static void main(String[] args) {

    }

    public int maxAreaOfIsland(int[][] grid) {
        int res = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 1) {
                    res = Math.max(res, helper(grid, i, j, grid.length, grid[0].length));
                }
            }
        }
        return res;
    }

    int helper(int[][] grid, int i, int j, int rows, int cols) {
        if (i < 0 || i >= rows || j < 0 || j >= cols || grid[i][j] == 0) return 0;
        grid[i][j] = 0;
        return 1 + helper(grid, i - 1, j, rows, cols) +
                helper(grid, i + 1, j, rows, cols) +
                helper(grid, i, j - 1, rows, cols) +
                helper(grid, i, j + 1, rows, cols);
    }
}