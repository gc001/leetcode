package com.gc.depthOrBreathSearch.q200;

/**
 * @program: leetcode
 * @description: 岛屿数量
 * @author: gc
 * @create: 2022-05-15 11:43
 */
public class Solution {

    public int numIslands(char[][] grid) {
        int res = 0;
        int m = grid.length;
        int n = grid[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    helper(grid, i, j, m, n);
                    res++;
                }
            }
        }
        return res;
    }

    void helper(char[][] grid, int i, int j, int m, int n) {
        if (i >= m || i < 0 || j >= n || j < 0 || grid[i][j] == '0') return;
        grid[i][j] = '0';
        helper(grid, i - 1, j, m, n);
        helper(grid, i + 1, j, m, n);
        helper(grid, i, j - 1, m, n);
        helper(grid, i, j + 1, m, n);
    }
}