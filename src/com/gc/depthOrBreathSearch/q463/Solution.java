package com.gc.depthOrBreathSearch.q463;

/**
 * @program: leetcode
 * @description: 岛屿的周长
 * @author: gc
 * @create: 2022-05-15 14:19
 */
public class Solution {
    public int islandPerimeter(int[][] grid) {
        if(grid==null || grid.length==0 || grid[0].length==0)
            return 0;
        int rows = grid.length;
        int cols = grid[0].length;
        int res = 0;
        for(int i=0;i<rows;i++)
            for(int j=0;j<cols;j++){
                if(grid[i][j]==1){
                    res +=4;
                    if(i<rows-1 && grid[i+1][j]==1)
                        res -=2;
                    if(j<cols-1 && grid[i][j+1]==1)
                        res -=2;
                }

            }
        return res;
    }
}