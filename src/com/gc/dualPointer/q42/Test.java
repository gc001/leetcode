package com.gc.dualPointer.q42;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 11:40
 */
public class Test {
    public int trap(int[] height) {
        if (height.length <= 2) return 0;
        int res = 0;
        int[] left = new int[height.length];
        for (int i = 1; i < height.length; i++) {
            left[i] = Math.max(left[i - 1], height[i - 1]);
        }
        int rightMax = height[height.length - 1];
        for (int i = height.length - 2; i >= 0; i--) {
            rightMax = Math.max(rightMax, height[i + 1]);
            int h = Math.min(rightMax, left[i]);
            if (h > height[i]) res += h-height[i];
        }
        return res;
    }
}