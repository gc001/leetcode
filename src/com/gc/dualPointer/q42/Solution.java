package com.gc.dualPointer.q42;

/**
 * @program: leetcode
 * @description: 接雨水
 * @author: gc
 * @create: 2022-04-12 20:52
 */
class Solution {
    public int trap(int[] height) {
        if (height.length <= 2) return 0;
        int res = 0;
        int[] arr = new int[height.length];
        for (int i = 1; i < height.length; i++) {
            arr[i] = Math.max(arr[i - 1], height[i - 1]);
        }
        int rightMax = height[height.length - 1];
        for (int i = height.length - 2; i > 0; i--) {
            rightMax = Math.max(rightMax, height[i + 1]);
            int min = Math.min(rightMax, arr[i]);
            int diff = min - height[i];
            res += Math.max(0, diff);
        }
        return res;
    }
}