package com.gc.dualPointer.q11;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-29 10:47
 */
public class Test {
    public int maxArea(int[] height) {
        if (height.length < 2) return 0;
        int res = 0;
        int l = 0;
        int r = height.length - 1;
        while (l < r) {
            res = Math.max(res, Math.min(height[l], height[r]) * (r - l));
            if (height[l] < height[r]) l++;
            else r--;
        }
        return res;
    }
}