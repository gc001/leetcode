package com.gc.dualPointer.q11;

/**
 * @program: leetcode
 * @description: 盛最多水的容器
 * @author: gc
 * @create: 2022-04-12 20:52
 */
public class Solution {
    public int maxArea(int[] height) {
        if (height.length <= 1) return 0;
        int res = 0;
        int left = 0;
        int right = height.length - 1;
        while (left < right) {
            int l = right - left;
            int w = Math.min(height[left], height[right]);
            res = Math.max(l * w, res);
            if (height[left] > height[right]) {
                right--;
            } else {
                left++;
            }
        }
        return res;
    }
}