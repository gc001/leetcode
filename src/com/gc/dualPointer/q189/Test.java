package com.gc.dualPointer.q189;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 10:22
 */
public class Test {
    public void rotate(int[] nums, int k) {
        k %= nums.length;
        if (k == 0 || nums.length <= 1) return;
        reverse(0, nums.length-1, nums);
        reverse(0, k - 1, nums);
        reverse(k, nums.length - 1, nums);
    }

    void reverse(int start, int end, int[] nums) {
        if (start >= end) return;
        while (start < end) {
            int t = nums[end];
            nums[end] = nums[start];
            nums[start] = t;
            start++;
            end--;
        }
    }
}