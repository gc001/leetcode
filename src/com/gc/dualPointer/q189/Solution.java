package com.gc.dualPointer.q189;

/**
 * @program: leetcode
 * @description: 轮转数组
 * @author: gc
 * @create: 2022-04-16 17:28
 */
public class Solution {
    public void rotate(int[] nums, int k) {
        if (k <= 0 || nums.length <= 1) return;
        k %= nums.length;
        rotate(nums, 0, nums.length - 1);
        rotate(nums, 0, k - 1);
        rotate(nums, k, nums.length - 1);
    }

    void rotate(int[] num, int start, int end) {
        if (start >= end) return;
        while (start < end) {
            int t = num[start];
            num[start] = num[end];
            num[end] = t;
            start++;
            end--;
        }
    }
}