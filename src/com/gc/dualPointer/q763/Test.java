package com.gc.dualPointer.q763;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 10:43
 */
public class Test {
    public List<Integer> partitionLabels(String s) {
        Map<Character, Integer> map = new HashMap<>();
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            map.put(s.charAt(i), i);
        }

        int idx = -1;
        // 起始点左侧
        int start = -1;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            idx = Math.max(idx, map.get(c));
            if (idx == i) {
                list.add(i - start );
                start = i ;
            }
        }
        return list;
    }
}