package com.gc.dualPointer.q763;

import java.util.*;

/**
 * @program: leetcode
 * @description: 划分字母区间
 * @author: gc
 * @create: 2022-04-17 16:09
 */
public class Solution {
    public static void main(String[] args) {
        System.out.println(new Solution().partitionLabels("ababcbacadefegdehijhklij"));
    }

    public List<Integer> partitionLabels(String s) {
        if (s.length() == 1) return Collections.singletonList(1);
        ArrayList<Integer> list = new ArrayList<>();
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            map.put(c, i);
        }
        int max = -1;
        int start = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            int idx = map.get(ch);
            // 当前以及之前字母出现的最后的位置
            max = Math.max(max, idx);
            if (i == max) {
                list.add(idx - start + 1);
                start = i + 1;
            }
        }
        return list;
    }
}