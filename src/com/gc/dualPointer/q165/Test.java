package com.gc.dualPointer.q165;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 11:22
 */
public class Test {
    public int compareVersion(String version1, String version2) {
        String[] ss1 = version1.split("\\.");
        String[] ss2 = version2.split("\\.");
        for (int i = 0; i < ss1.length || i < ss2.length; i++) {
            int v1 = 0, v2 = 0;
            if (i < ss1.length) v1 = Integer.parseInt(ss1[i]);
            if (i < ss2.length) v2 = Integer.parseInt(ss2[i]);
            if (v1 == v2) continue;
            return v1 > v2 ? 1 : -1;
        }
        return 0;
    }
}