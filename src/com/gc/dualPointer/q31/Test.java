package com.gc.dualPointer.q31;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 12:34
 */
public class Test {
    public void nextPermutation(int[] nums) {
        if (nums.length <= 1) return;
        int i = nums.length - 2;
        while (i >= 0 && nums[i+1] <= nums[i]) i--;
        if (i == -1) {
            reverse(nums, 0, nums.length - 1);
            return;
        }
        int j = nums.length - 1;
        while (j > 0 && nums[j] <= nums[i]) j--;
        swap(nums, i, j);
        reverse(nums, i + 1, nums.length - 1);
    }

    void swap(int[] arr, int i, int j) {
        int t = arr[j];
        arr[j] = arr[i];
        arr[i] = t;
    }

    void reverse(int[] arr, int i, int j) {
        while (i <= j) {
            int t = arr[j];
            arr[j] = arr[i];
            arr[i] = t;
            i++;
            j--;
        }
    }
}