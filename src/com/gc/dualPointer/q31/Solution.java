package com.gc.dualPointer.q31;

/**
 * @program: leetcode
 * @description: 下一个排列
 * @author: gc
 * @create: 2022-05-21 17:19
 */
public class Solution {
    public void nextPermutation(int[] nums) {
        if (nums.length <= 1) return;
        int i = nums.length - 1;
        while (i > 0 && nums[i] <= nums[i - 1]) i--;
        if (i == 0) {
            reverse(nums, 0, nums.length - 1);
            return;
        }
        i--;
        int j = nums.length - 1;
        while (j > 0 && nums[j] <= nums[i]) j--;
        swap(nums, i, j);
        reverse(nums, i + 1, nums.length - 1);
    }

    void swap(int[] arr, int i, int j) {
        int t = arr[j];
        arr[j] = arr[i];
        arr[i] = t;
    }

    void reverse(int[] arr, int i, int j) {
        while (i <= j) {
            int t = arr[j];
            arr[j] = arr[i];
            arr[i] = t;
            i++;
            j--;
        }
    }
}