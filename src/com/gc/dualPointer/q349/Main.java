package com.gc.dualPointer.q349;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-04-16 16:32
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        Solution1 solution1 = new Solution1();
        int n = 1000;
        Random random = new Random();
        int[] nums1 = new int[n];
        int[] nums2 = new int[n];
        for (int i = 0; i < n; i++) {
            nums1[i] = random.nextInt(n);
            nums2[i] = random.nextInt(n);
        }
        long startTime = System.currentTimeMillis();
        int[] arr1 = solution.intersection(nums1, nums2);
        System.out.println("set 用时： " + (System.currentTimeMillis() - startTime));
        startTime = System.currentTimeMillis();
        int[] arr2 = solution1.intersection(nums1, nums2);
        System.out.println("arr 用时： " + (System.currentTimeMillis() - startTime));
    }
}