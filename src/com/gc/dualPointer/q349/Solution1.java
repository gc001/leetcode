package com.gc.dualPointer.q349;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 两个数组的交集
 * @author: gc
 * @create: 2022-04-16 16:27
 */
public class Solution1 {
    public int[] intersection(int[] nums1, int[] nums2) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (int i : nums1) {
            if (i > max) max = i;
            if (i < min) min = i;
        }
        int[] arr = new int[max - min + 1];
        for (int i : nums1) {
            arr[i - min] = 1;
        }
        List<Integer> list = new ArrayList<>();
        for (int i : nums2) {
            if (i > max || i < min || arr[i - min] == 0) continue;
            arr[i-min] = 0;
            list.add(i);
        }
        int[] res = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }
}