package com.gc.dualPointer.q15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-29 10:51
 */
public class Test {
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums.length < 3) {
            return new ArrayList<>();
        }
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) continue;
            int l = i + 1;
            int r = nums.length - 1;
            while (l < r) {
                int sum = nums[i] + nums[l] + nums[r];
                if (sum == 0) {
                    list.add(Arrays.asList(nums[i], nums[l], nums[r]));
                    l++;
                } else if (sum > 0) {
                    r--;
                } else {
                    l++;
                }
                while (l < r && l > i + 1 && nums[l] == nums[l - 1]) l++;
                while (l < r && r < nums.length - 1 && nums[r] == nums[r + 1]) r--;
            }
        }
        return list;
    }
}