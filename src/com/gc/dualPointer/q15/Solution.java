package com.gc.dualPointer.q15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: leetcode
 * @description: 三数之和
 * @author: gc
 * @create: 2022-04-13 21:14
 */
public class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        if (nums == null || nums.length <= 2) return list;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i - 1])
                continue;
            int left = i + 1;
            int right = nums.length - 1;
            while (left < right) {
                int sum = nums[left] + nums[right] + nums[i];
                if (sum == 0) {
                    list.add(Arrays.asList(nums[left], nums[right], nums[i]));
                    left++;
                } else if (sum > 0) {
                    right--;
                } else {
                    left++;
                }
                while (left < right && left > i + 1 && nums[left] == nums[left - 1])
                    left++;
                while (right > left && right < nums.length - 1 && nums[right] == nums[right + 1])
                    right--;
            }
        }
        return list;
    }
}