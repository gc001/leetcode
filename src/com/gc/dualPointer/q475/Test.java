package com.gc.dualPointer.q475;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 11:10
 */
public class Test {
    public int findRadius(int[] houses, int[] heaters) {
        Arrays.sort(heaters);
        Arrays.sort(houses);
        int i = 0;
        int res = 0;
        for (int houseIdx : houses) {
            while (i < heaters.length && heaters[i] < houseIdx) {
                i++;
            }
            if (i == 0) res = Math.max(res, heaters[i] - houseIdx);
            else if (i == heaters.length) res = Math.max(res, houses[houses.length - 1] - heaters[heaters.length - 1]);
            else res = Math.max(res, Math.min(heaters[i] - houseIdx, houseIdx - heaters[i - 1]));
        }
        return res;
    }
}