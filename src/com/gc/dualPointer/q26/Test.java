package com.gc.dualPointer.q26;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-29 11:56
 */
public class Test {
    public int removeDuplicates(int[] nums) {
        int idx = 0;
        for (int i = 1; i < nums.length; i++) {
            if(nums[idx]!=nums[i])
                nums[++idx] = nums[i];
        }
        return idx+1;
    }
}