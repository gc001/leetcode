package com.gc.dualPointer.q26;

/**
 * @program: leetcode
 * @description: 删除有序数组中的重复项
 * @author: gc
 * @create: 2022-04-14 20:49
 */
public class Solution {
    public int removeDuplicates(int[] nums) {
        if (nums.length <= 1) return nums.length;
        int idx = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            nums[idx++] = nums[i];
        }
        return idx;
    }
}