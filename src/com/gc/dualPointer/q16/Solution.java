package com.gc.dualPointer.q16;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 最接近的三数之和
 * @author: gc
 * @create: 2022-04-14 21:01
 */
public class Solution {
    public static void main(String[] args) {
        System.out.println(new Solution().threeSumClosest(new int[]{-3, -2, -5, 3, -4}, -1));
    }

    public int threeSumClosest(int[] nums, int target) {
        long res = Integer.MAX_VALUE;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            int left = i + 1;
            int right = nums.length - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                if (sum == target) return sum;
                if (Math.abs(sum - target) < Math.abs(res - target)) {
                    res = sum;
                }
                if (sum > target) {
                    right--;
                } else {
                    left++;
                }
            }
        }
        return (int) res;
    }
}