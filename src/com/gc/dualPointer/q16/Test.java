package com.gc.dualPointer.q16;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-29 11:02
 */
public class Test {
    public int threeSumClosest(int[] nums, int target) {
        long res = Integer.MAX_VALUE;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            int l = i + 1;
            int r = nums.length - 1;
            while (l < r) {
                int sum = nums[i] + nums[l] + nums[r];
                if (sum == target) return sum;
                if (Math.abs(sum - target) < Math.abs(res - target)) {
                    res = sum;
                }
                if (sum > target) r--;
                else l++;
            }
        }
        return (int) res;
    }
}