package com.gc.dualPointer.q567;

/**
 * @program: leetcode
 * @description: 字符串的排列
 * @author: gc
 * @create: 2022-04-17 12:28
 */
public class Solution {
    public boolean checkInclusion(String s1, String s2) {
        int[] arr = new int[26];
        int cnt = s1.length();
        for (char c : s1.toCharArray()) {
            arr[c - 'a']++;
        }
        int i = 0;
        int j = 0;
        while (j < s2.length()) {
            char ch = s2.charAt(j);
            if (arr[ch - 'a'] > 0) {
                cnt--;
            }
            if (cnt == 0) return true;
            arr[ch - 'a']--;
            j++;
            if (j - i >= s1.length()) {
                ch = s2.charAt(i);
                if (arr[ch - 'a'] >= 0) {
                    cnt++;
                }
                arr[ch - 'a']++;
                i++;
            }
        }
        return false;
    }
}