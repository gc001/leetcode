package com.gc.dualPointer.q567;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 10:49
 */
public class Test {
    public boolean checkInclusion(String s1, String s2) {
        int[] arr = new int[26];
        for (int i = 0; i < s1.length(); i++) {
            arr[s1.charAt(i) - 'a']++;
        }
        int i = 0;
        int j = 0;
        int cnt = s1.length();
        while (j < s2.length()) {
            char ch = s2.charAt(j);
            if (arr[ch - 'a'] >= 1) cnt--;
            if (cnt == 0) return true;
            arr[ch - 'a']--;
            j++;
            if (j - i >= s1.length()) {
                ch = s2.charAt(i);
                if (arr[ch - 'a'] >= 0) {
                    cnt++;
                }
                arr[ch - 'a']++;
                i++;
            }
        }
        return false;
    }
}