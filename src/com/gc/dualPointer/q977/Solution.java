package com.gc.dualPointer.q977;

/**
 * @program: leetcode
 * @description: 有序数组的平方
 * @author: gc
 * @create: 2022-04-17 15:18
 */
public class Solution {
    public int[] sortedSquares(int[] nums) {
        int i = 0;
        int j = nums.length - 1;
        int idx = j;
        int[] arr = new int[nums.length];
        while (i <= j) {
            if (Math.abs(nums[i]) > Math.abs(nums[j])) {
                arr[idx] = nums[i] * nums[i];
                idx--;
                i++;
            } else {
                arr[idx] = nums[j] * nums[j];
                idx--;
                j--;
            }
        }
        return arr;
    }
}