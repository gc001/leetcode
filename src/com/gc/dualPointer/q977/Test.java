package com.gc.dualPointer.q977;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-29 12:01
 */
public class Test {
    public int[] sortedSquares(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        int[] arr = new int[nums.length];
        int idx = nums.length - 1;
        while (l <= r) {
            if (Math.abs(nums[l]) < Math.abs(nums[r])) {
                arr[idx--] = nums[r] * nums[r];
                r--;
            } else {
                arr[idx--] = nums[l] * nums[l];
                l++;
            }
        }
        return arr;
    }
}