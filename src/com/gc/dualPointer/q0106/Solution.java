package com.gc.dualPointer.q0106;

/**
 * @program: leetcode
 * @description: 字符串压缩
 * @author: gc
 * @create: 2022-04-17 15:29
 */
public class Solution {
    public String compressString(String s) {
        if (s == null || s.length() <= 1) return s;
        int i = 0;
        int j = 0;
        StringBuilder sb = new StringBuilder();
        while (j < s.length()) {
            while (j < s.length() && s.charAt(j) == s.charAt(i)) {
                j++;
            }
            sb.append(s.charAt(i)).append(j - i);
            i = j;
        }
        return sb.length() >= s.length() ? s : sb.toString();
    }
}