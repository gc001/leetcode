package com.gc.dualPointer.q0106;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 11:30
 */
public class Test {
    public String compressString(String s) {
        if (s == null || s.length() <= 1) return s;
        int cnt = 0;
        int l = 0;
        int r = 0;
        StringBuilder sb = new StringBuilder();
        while (r < s.length()) {
            if (s.charAt(l) == s.charAt(r)) {
                cnt++;
                r++;
            } else {
                sb.append(s.charAt(l)).append(cnt);
                l = r;
                cnt = 0;
            }
        }
        sb.append(s.charAt(l)).append(cnt);
        return sb.length() >= s.length() ? s : sb.toString();
    }
}