package com.gc.dualPointer.q18;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: leetcode
 * @description: 四数之和
 * @author: gc
 * @create: 2022-04-16 16:46
 */
public class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> list = new ArrayList<>();
        if (nums == null || nums.length <= 3) return list;
        Arrays.sort(nums);
        for (int j = 0; j < nums.length - 3; j++) {
            if (j > 0 && nums[j] == nums[j - 1])
                continue;
            if ((long) nums[j] + nums[j + 1] + nums[j + 2] + nums[j + 3] > target) {
                break;
            }
            if ((long) nums[j] + nums[nums.length - 3] + nums[nums.length - 2] + nums[nums.length - 1] < target) {
                continue;
            }
            for (int i = j + 1; i < nums.length - 2; i++) {
                if (i > j + 1 && nums[i] == nums[i - 1])
                    continue;
                int left = i + 1;
                int right = nums.length - 1;
                while (left < right) {
                    int sum = nums[left] + nums[right] + nums[i] + nums[j];
                    if (sum == target) {
                        list.add(Arrays.asList(nums[left], nums[right], nums[i], nums[j]));
                        left++;
                    } else if (sum > target) {
                        right--;
                    } else {
                        left++;
                    }
                    while (left < right && left > i + 1 && nums[left] == nums[left - 1])
                        left++;
                    while (right > left && right < nums.length - 1 && nums[right] == nums[right + 1])
                        right--;
                }
            }
        }
        return list;
    }
}