package com.gc.dualPointer.q167;

/**
 * @program: leetcode
 * @description: 两数之和 II - 输入有序数组
 * @author: gc
 * @create: 2022-04-16 20:07
 */
public class Solution {
    public int[] twoSum(int[] numbers, int target) {
        int left = 0;
        int right = numbers.length - 1;
        int[] res = new int[2];
        while (left < right) {
            if (left > 0 && numbers[left] == numbers[left - 1]) left++;
            else if (right < numbers.length - 1 && numbers[right] == numbers[right + 1]) right--;
            else if (numbers[left] + numbers[right] == target) return new int[]{left, right};
            else if (numbers[left] + numbers[right] > target) right--;
            else left++;
        }
        return res;
    }
}