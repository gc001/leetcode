package com.gc.dualPointer.q167;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-29 12:05
 */
public class Test {
    public int[] twoSum(int[] numbers, int target) {
        int[] res = new int[2];
        int l = 0;
        int r = numbers.length-1;
        while (l<r){
            if(l>0 && numbers[l]==numbers[l-1]) l++;
            else if(r< numbers.length-1&&numbers[r]==numbers[r+1])r--;
            else if(numbers[l]+numbers[r]==target) return new int[]{l+1,r+1};
            else if(numbers[l]+numbers[r]>target) r--;
            else l++;
        }
        return res;
    }
}