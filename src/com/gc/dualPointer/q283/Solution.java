package com.gc.dualPointer.q283;

/**
 * @program: leetcode
 * @description: 移动零
 * @author: gc
 * @create: 2022-04-13 22:09
 */
class Solution {
    public void moveZeroes(int[] nums) {
        if (nums == null || nums.length <= 1) return;
        int idx = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[idx] = nums[i];
                if (i != idx)
                    nums[i] = 0;
                idx++;
            }
        }
    }
}