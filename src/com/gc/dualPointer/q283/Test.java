package com.gc.dualPointer.q283;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 10:27
 */
public class Test {
    public void moveZeroes(int[] nums) {
        int idx = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[idx] = nums[i];
                if (idx != i) {
                    nums[i] = 0;
                }
                idx++;
            }
        }
    }
}