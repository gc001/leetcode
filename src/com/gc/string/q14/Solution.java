package com.gc.string.q14;

/**
 * @program: leetcode
 * @description: 最长公共前缀
 * @author: gc
 * @create: 2022-04-19 21:02
 */
public class Solution {
    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 1) return strs[0];
        int max = 0;
        boolean flag = false;
        for (int i = 0; i < strs[0].length(); i++) {
            char ch1 = strs[0].charAt(i);
            for (int j = 1; j < strs.length; j++) {
                if (i >= strs[j].length()) {
                    flag = true;
                    break;
                }
                char ch2 = strs[j].charAt(i);
                if (ch1 != ch2) {
                    flag = true;
                    break;
                }
            }
            if (flag) break;
            max++;
        }
        return strs[0].substring(0, max + 1);
    }
}