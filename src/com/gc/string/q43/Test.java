package com.gc.string.q43;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 15:50
 */
public class Test {
    public String multiply(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) return "0";
        int[] arr = new int[num1.length() + num2.length()];
        for (int i = num1.length() - 1; i >= 0; i--) {
            for (int j = num2.length() - 1; j >= 0; j--) {
                arr[i + j + 1] += (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
            }
        }
        for (int i = arr.length - 1; i >= 1; i--) {
            if (arr[i] >= 10) {
                arr[i - 1] += arr[i] / 10;
                arr[i] %= 10;
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int n : arr) {
            if (n == 0 && sb.length() == 0) {
                continue;
            }
            sb.append(n);
        }
        return sb.toString();
    }
}