package com.gc.string.q43;

/**
 * @program: leetcode
 * @description: 字符串相乘
 * @author: gc
 * @create: 2022-04-20 21:56
 */

public class Solution {

    public String multiply(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) return "0";
        int[] res = new int[num1.length() + num2.length()];
        for (int i = num1.length() - 1; i >= 0; i--) {
            for (int j = num2.length() - 1; j >= 0; j--) {
                res[i + j + 1] += (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
            }
        }
        for (int i = res.length - 1; i > 0; i--) {
            if (res[i] >= 10) {
                res[i - 1] += res[i] / 10;
                res[i] %= 10;
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int n : res) {
            if (n == 0 && sb.length() == 0) {
                continue;
            }
            sb.append(n);
        }
        return sb.toString();
    }
}