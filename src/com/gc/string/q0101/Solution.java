package com.gc.string.q0101;

/**
 * @program: leetcode
 * @description: 判定字符是否唯一
 * @author: gc
 * @create: 2022-04-20 20:38
 */
public class Solution {
    public boolean isUnique(String astr) {
        int[] arr = new int[256];
        for (char c : astr.toCharArray()) {
            if (arr[c - '0'] != 0) return false;
            arr[c - '0']++;
        }
        return true;
    }
}