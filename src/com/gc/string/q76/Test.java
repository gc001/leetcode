package com.gc.string.q76;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 15:22
 */
public class Test {
    public String minWindow(String s, String t) {
        int[] arr = new int[126];
        for (int i = 0; i < t.length(); i++) {
            arr[t.charAt(i)]++;
        }
        int left = 0, right = 0, start = 0, end = 0, cnt = t.length(), min = s.length() + 1;
        while (right < s.length()) {
            if (arr[s.charAt(right)] > 0) cnt--;
            arr[s.charAt(right)]--;
            right++;
            while (cnt == 0) {
                if (min > right - left) {
                    start = left;
                    end = right;
                    min = right - left;
                }
                if (arr[s.charAt(left)] >= 0) {
                    cnt++;
                }
                arr[s.charAt(left)]++;
                left++;
            }

        }
        return s.substring(start, end);
    }
}