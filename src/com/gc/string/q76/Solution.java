package com.gc.string.q76;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 最小覆盖子串
 * @author: gc
 * @create: 2022-04-23 20:55
 */
public class Solution {
    public String minWindow(String s, String t) {
        if (s.length() < t.length()) {
            return "";
        }
        int left = 0, right = 0, min = Integer.MAX_VALUE, resL = 0, resR = 0, cnt = t.length();
        int[] arr = new int[128];
        for (int i = 0; i < t.length(); i++) {
            arr[t.charAt(i)]++;
        }
        while (right < s.length()) {
            if (arr[s.charAt(right)] > 0) {
                cnt--;
            }
            arr[s.charAt(right)]--;
            right++;
            while (cnt == 0) {
                if (min > right - left) {
                    min = right - left;
                    resL = left;
                    resR = right;
                }
                if (arr[s.charAt(left)] >= 0) {
                    cnt++;
                }
                arr[s.charAt(left)]++;
                left++;
            }
        }
        return s.substring(resL, resR);
    }
}















