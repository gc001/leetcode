package com.gc.string.q20;

import java.util.Stack;

/**
 * @program: leetcode
 * @description: 有效的括号
 * @author: gc
 * @create: 2022-04-18 20:55
 */
public class Solution {
    public boolean isValid(String s) {
        if (s.length() % 2 == 1) return false;
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c == '[' || c == '(' || c == '{') {
                stack.push(c);
            } else {
                if (stack.isEmpty()) return false;
                Character pop = stack.pop();
                if ((c == ']' && pop != '[')
                        || (c == ')' && pop != '(')
                        || (c == '}' && pop != '{')) return false;
            }
        }
        return stack.isEmpty();
    }
}