package com.gc.string.q13;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: leetcode
 * @description: 罗马数字转整数
 * @author: gc
 * @create: 2022-04-19 21:20
 */
public class Solution {
    public int romanToInt(String s) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);
        map.put('L', 50);
        map.put('C', 100);
        map.put('D', 500);
        map.put('M', 1000);
        int res = 0;
        for (int i = 0; i < s.length(); ) {
            if (i == s.length() - 1) {
                res += map.get(s.charAt(i));
                break;
            }
            if (s.charAt(i) == 'I') {
                if (s.charAt(i + 1) == 'V' || s.charAt(i + 1) == 'X') {
                    res += map.get(s.charAt(i + 1)) - 1;
                    i += 2;
                } else {
                    res += 1;
                    i++;
                }
            } else if (s.charAt(i) == 'X') {
                if (s.charAt(i + 1) == 'L' || s.charAt(i + 1) == 'C') {
                    res += map.get(s.charAt(i + 1)) - 10;
                    i += 2;
                } else {
                    res += 10;
                    i++;
                }
            } else if (s.charAt(i) == 'C') {
                if (s.charAt(i + 1) == 'D' || s.charAt(i + 1) == 'M') {
                    res += map.get(s.charAt(i + 1)) - 100;
                    i += 2;
                } else {
                    res += 100;
                    i++;
                }
            } else {
                res += map.get(s.charAt(i));
                i++;
            }
        }
        return res;

    }
}