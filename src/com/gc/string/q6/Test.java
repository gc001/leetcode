package com.gc.string.q6;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-06-11 21:55
 */
public class Test {

    public static void main(String[] args) {
        new Test().convert("PAYPALISHIRING", 3);
    }

    public String convert(String s, int numRows) {
        if (s.length() <= 1 || numRows <= 1) return s;
        StringBuilder[] sbs = new StringBuilder[numRows];
        int idx = -1;
        boolean f = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (f) {
                idx++;
                if (idx == numRows - 1) f = false;
            } else {
                idx--;
                if (idx == 0) f = true;
            }
            if (sbs[idx] == null) sbs[idx] = new StringBuilder();
            sbs[idx].append(s.charAt(i));
        }
        for (StringBuilder part : sbs) {
            if (part != null)
                sb.append(part);
        }
        return sb.toString();
    }
}