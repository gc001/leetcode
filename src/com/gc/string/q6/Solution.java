package com.gc.string.q6;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: Z 字形变换
 * @author: gc
 * @create: 2022-04-20 20:58
 */
public class Solution {

    public String convert(String s, int numRows) {
        if (s.length() <= 1 || numRows <= 1) return s;
        StringBuffer[] arr = new StringBuffer[numRows];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new StringBuffer();
        }
        int i = 0;
        int idx = -1;
        boolean f = true;
        while (i < s.length()) {
            if (f) {
                idx++;
                if (idx == numRows - 1)
                    f = false;
            } else {
                idx--;
                if (idx == 0)
                    f = true;
            }

            arr[idx].append(s.charAt(i));
            i++;
        }
        StringBuilder res = new StringBuilder();
        for (StringBuffer sb : arr) {
            res.append(sb);
        }
        return res.toString();
    }
}