package com.gc.string.q71;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * @program: leetcode
 * @description: 简化路径
 * @author: gc
 * @create: 2022-04-18 21:17
 */
public class Solution {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.simplifyPath("/a/.././b/c/./d"));
    }

    public String simplifyPath(String path) {
        if (path.length() == 1) return path;
        Deque<String> dq = new LinkedList<>();
        StringBuilder sb = new StringBuilder();
        for (String p : path.split("/")) {
            if (p.equals("..")) {
                dq.pollLast();
            } else if (!p.equals(".") && p.length() != 0) {
                dq.offerLast(p);
            }
        }
        while (!dq.isEmpty()) {
            sb.append("/").append(dq.pollFirst());
        }
        return sb.toString().length() == 0 ? "/" : sb.toString();
    }
}