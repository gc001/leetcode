package com.gc.string.q71;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 15:36
 */
public class Test {

    public static void main(String[] args) {
        System.out.println(new Test().simplifyPath("/home/"));
    }

    public String simplifyPath(String path) {
        Deque<String> dq = new LinkedList<>();
        if (path.length() == 1) {
            return path;
        }
        for (String p : path.split("/")) {
            if (p.equals("..")) {
                dq.pollLast();
            } else if (!p.equals(".") && p.length() != 0) {
                dq.offerLast(p);
            }
        }
        StringBuilder sb = new StringBuilder();
        while (!dq.isEmpty()) {
            sb.append("/").append(dq.pollFirst());
        }
        return sb.length() == 0 ? "" : sb.toString();
    }
}