package com.gc.string.q49;

import java.util.*;

/**
 * @program: leetcode
 * @description: 字母异位词分组
 * @author: gc
 * @create: 2022-04-23 20:24
 */
public class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] chs = str.toCharArray();
            Arrays.sort(chs);
            String t = new String(chs);
            if (map.containsKey(t)) {
                List<String> list = map.get(t);
                map.get(t).add(str);
            } else {
                List<String> sub = new ArrayList<>();
                sub.add(str);
                map.put(t,sub);
            }
        }
        return new ArrayList<>(map.values());
    }
}