package com.gc.string.q5;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 23:11
 */
public class Test {

    int start = 0, end = 0;

    public String longestPalindrome(String s) {
        for (int i = 0; i < s.length(); i++) {
            helper(i, i, s);
            helper(i, i + 1, s);
        }
        return s.substring(start, end + 1);
    }

    void helper(int left, int right, String s) {
        while (left >= 0 && right < s.length()) {
            if (s.charAt(left) != s.charAt(right)) return;
            if (right - left > end - start) {
                start = left;
                end = right;
            }
            left--;
            right++;
        }
    }
}