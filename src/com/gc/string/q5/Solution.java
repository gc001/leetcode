package com.gc.string.q5;

/**
 * @program: leetcode
 * @description: 最长回文子串
 * @author: gc
 * @create: 2022-04-18 20:39
 */
public class Solution {

    int start = 0;
    int end = 0;

    public String longestPalindrome(String s) {
        if (s == null || s.length() <= 1) return s;
        for (int i = 0; i < s.length(); i++) {
            helper(s, i, i);
            helper(s, i, i + 1);
        }
        return s.substring(start, end + 1);
    }

    void helper(String s, int left, int right) {
        while (left >= 0 && right < s.length()) {
            if (s.charAt(left) != s.charAt(right)) {
                break;
            }
            if (right - left > end - start) {
                start = left;
                end = right;
            }
            left--;
            right++;
        }
    }
}