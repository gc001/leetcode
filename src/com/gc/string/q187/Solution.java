package com.gc.string.q187;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * @program: leetcode
 * @description: 重复的DNA序列
 * @author: gc
 * @create: 2022-04-18 21:06
 */
public class Solution {
    public List<String> findRepeatedDnaSequences(String s) {
        if (s.length() < 10) return new ArrayList<>();
        HashSet<String> set = new HashSet<>();
        HashSet<String> res = new HashSet<>();
        int left = 0;
        int right = 9;
        while (right < s.length()) {
            String sub = s.substring(left, right + 1);
            if (!set.add(sub)) {
                res.add(sub);
            }
            right++;
            left++;
        }
        return new ArrayList<>(res);
    }
}