package com.gc.string.q3;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 无重复字符的最长子串
 * @author: gc
 * @create: 2022-04-20 21:45
 */
public class Solution {
    public int lengthOfLongestSubstring(String s) {
        if (s.length() <= 0) return 0;
        int[] arr = new int[256];
        Arrays.fill(arr, -1);
        int left = -1;
        int res = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            left = Math.max(left, arr[c]);
            res = Math.max(res, i - left);
            arr[c] = i;
        }
        return res;
    }
}