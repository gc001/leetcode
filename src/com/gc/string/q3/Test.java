package com.gc.string.q3;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 22:58
 */
public class Test {
    public int lengthOfLongestSubstring(String s) {
        if (s.length() <= 1) return s.length();
        int[] arr = new int[256];
        for (int i = 0; i < s.length(); i++) {
            arr[s.charAt(i)] = -1;
        }
        int left = -1;
        int res = 1;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            left = Math.max(left, arr[ch]);
            res = Math.max(res, i - left);
            arr[ch] = i;
        }
        return res;
    }
}