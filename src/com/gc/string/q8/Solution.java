package com.gc.string.q8;

/**
 * @program: leetcode
 * @description: 字符串转换整数 (atoi)
 * @author: gc
 * @create: 2022-04-21 21:32
 */
public class Solution {
    public int myAtoi(String s) {
        if(s.length()==0) return 0;
        int idx = 0;
        long res = 0;
        int flag = 1;
        // 前面的0
        while (idx < s.length() && s.charAt(idx) == ' ') {
            idx++;
            if (idx >= s.length()) return 0;
        }
        // 符号
        if (s.charAt(idx) == '+') {
            idx++;
        } else if (s.charAt(idx) == '-') {
            flag = -1;
            idx++;
        }
        while (idx < s.length()) {
            char c = s.charAt(idx);
            if (isNum(c)) {
                res = res * 10 + (c - '0');
                idx++;
                if (res * flag >= Integer.MAX_VALUE) return Integer.MAX_VALUE;
                else if (res * flag <= Integer.MIN_VALUE) return Integer.MIN_VALUE;
            } else {
                break;
            }
        }
        return (int) res * flag;
    }

    boolean isNum(char ch) {
        return ch - '0' >= 0 && ch - '0' <= 9;
    }
}