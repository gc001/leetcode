package com.gc.stack.q32;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 18:10
 */
public class Test {
    public int longestValidParentheses(String s) {
        if (s == null || s.length() <= 1) return 0;
        Deque<Integer> stack = new LinkedList<>();
        int[] arr = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                stack.push(i);
            } else {
                if (stack.isEmpty()) continue;
                int preIdx = stack.pop();
                arr[i] = 1;
                arr[preIdx] = 1;
            }
        }
        int res = 0;
        int max = 0;
        for (int n : arr) {
            if (n == 1) {
                max++;
                res = Math.max(res, max);
            } else {
                max = 0;
            }
        }
        return res;
    }
}