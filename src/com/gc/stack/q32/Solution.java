package com.gc.stack.q32;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description: 最长有效括号
 * @author: gc
 * @create: 2022-05-07 23:34
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().longestValidParentheses("(()"));
    }

    // 用栈模拟一遍，将所有可以匹配的括号的位置全部置1，则有0的部分肯定就是断开的部分，只要找到连续的1
    public int longestValidParentheses(String s) {
        if (s == null || s.length() <= 1) return 0;
        Deque<Integer> stack = new LinkedList<>();
        int[] arr = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(') {
                stack.push(i);
            } else {
                if (!stack.isEmpty()) {
                    int pre = stack.pop();
                    if (s.charAt(pre) == '(') {
                        arr[pre] = 1;
                        arr[i] = 1;
                    }
                }
            }
        }
        int res = 0;
        int max = 0;
        for (int x : arr) {
            if (x == 1) {
                max++;
                res = Math.max(res, max);
            } else {
                max = 0;
            }
        }
        return res;
    }
}