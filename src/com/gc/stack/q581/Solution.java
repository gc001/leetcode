package com.gc.stack.q581;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 最短无序连续子数组
 * @author: gc
 * @create: 2022-05-01 16:06
 */
public class Solution {
    public static void main(String[] args) {
        System.out.println(new Solution().findUnsortedSubarray(new int[]{1, 3, 5, 7, 2, 4, 6, 8}));
    }

    public int findUnsortedSubarray(int[] nums) {
        int[] arr = Arrays.copyOf(nums, nums.length);
        Arrays.sort(arr);
        int left = 0;
        int right = nums.length - 1;
        while (left < right) {
            if (arr[left] == nums[left]) {
                left++;
            }
            if (arr[right] == nums[right]) {
                right--;
            }
            if (arr[left] != nums[left] && arr[right] != nums[right]) {
                break;
            }
        }
        return right == left ? 0 : right - left + 1;
    }
}