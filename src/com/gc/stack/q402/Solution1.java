package com.gc.stack.q402;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description: 移掉 K 位数字
 * @author: gc
 * @create: 2022-04-30 16:30
 */
public class Solution1 {

    public static void main(String[] args) {
        System.out.println(new Solution1().removeKdigits("1432219", 3));
        System.out.println(new Solution1().removeKdigits("10200", 1));
        System.out.println(new Solution1().removeKdigits("100000", 3));
        System.out.println(new Solution1().removeKdigits("1357642", 4));
        System.out.println(new Solution1().removeKdigits("112", 1));
        System.out.println(new Solution1().removeKdigits("11111", 2));
    }

    public String removeKdigits(String num, int k) {
        if (k >= num.length()) return "0";
        if (k == 0) return num;
        int cnt = num.length() - k;
        Deque<Integer> q = new LinkedList<>();
        StringBuilder sb = new StringBuilder();
        String behind = "";
        for (int i = 0; i < num.length(); i++) {
            int n = num.charAt(i) - '0';
            while (!q.isEmpty() && q.peekLast() > n) {
                q.pollLast();
                k--;
                if (k == 0) {
                    behind = num.substring(i);
                    break;
                }
            }
            if (k == 0) break;
            q.addLast(n);
        }
        while (!q.isEmpty() && k > 0) {
            k--;
            q.pollLast();
        }
        while (!q.isEmpty()) sb.append(q.pollFirst());
        sb.append(behind);
        int i = 0;
        for (; i < sb.length(); i++) {
            if (sb.charAt(i) != '0') break;
        }
        return i == sb.length() ? "0" : sb.substring(i);
    }
}