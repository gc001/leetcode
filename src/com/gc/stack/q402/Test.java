package com.gc.stack.q402;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 20:25
 */
public class Test {
    public String removeKdigits(String num, int k) {
        if (k >= num.length()) return "0";
        if (k == 0) return num;
        int cnt = num.length() - k;
        Deque<Character> q = new LinkedList<>();
        StringBuilder sb = new StringBuilder();
        String behind = "";
        for (int i = 0; i < num.length(); i++) {
            while (!q.isEmpty() && q.peekLast() > num.charAt(i)) {
                q.pollLast();
                k--;
                if (k == 0) {
                    behind = num.substring(i);
                    break;
                }
            }
            if (k == 0) break;
            q.offerLast(num.charAt(i));
        }
        while (!q.isEmpty()) {
            sb.append(q.pollFirst());
            cnt--;
            if (cnt == 0) break;
        }
        sb.append(behind);
        int i = 0;
        for (; i < sb.length(); i++) {
            if (sb.charAt(i) != '0') break;
        }
        String res = sb.substring(i);
        return res.length() == 0 ? "0" : res;

    }
}