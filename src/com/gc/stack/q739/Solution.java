package com.gc.stack.q739;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description: 每日温度
 * @author: gc
 * @create: 2022-05-01 15:51
 */
public class Solution {
    public int[] dailyTemperatures(int[] arr) {
        int[] res = new int[arr.length];
        Deque<Integer> stack = new LinkedList<>();
        for (int i = 0; i < arr.length; i++) {
            while (!stack.isEmpty() && arr[i] > arr[stack.peek()]) {
                int preIdx = stack.pop();
                res[preIdx] = i - preIdx;
            }
            stack.push(i);
        }
        return res;
    }
}