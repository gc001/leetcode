package com.gc.stack.q739;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 17:59
 */
public class Test {
    public int[] dailyTemperatures(int[] temperatures) {
        int[] arr = new int[temperatures.length];
        Deque<Integer> stack = new LinkedList<>();
        for (int i = 0; i < temperatures.length; i++) {
            while (!stack.isEmpty() && temperatures[stack.peek()] < temperatures[i]) {
                int preIdx = stack.pop();
                arr[preIdx] = i - preIdx;
            }
            stack.push(i);
        }
        return arr;
    }
}