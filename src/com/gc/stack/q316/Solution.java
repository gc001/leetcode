package com.gc.stack.q316;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description: 去除重复字母
 * @author: gc
 * @create: 2022-04-30 21:53
 * @refer https://leetcode-cn.com/problems/remove-duplicate-letters/solution/yi-zhao-chi-bian-li-kou-si-dao-ti-ma-ma-zai-ye-b-4/
 */
public class Solution {
    public String removeDuplicateLetters(String s) {
        if (s.length() <= 1) return s;
        int[] arr = new int[26];
        for (int i = 0; i < s.length(); i++) {
            arr[s.charAt(i) - 'a']++;
        }
        HashSet<Character> set = new HashSet<>();
        Deque<Character> dq = new LinkedList<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            // 队列中尚不存在c的时候直接存入
            if (!set.contains(c)) {
                // 后面还存在dq.peekLast()这个字符，同时这个字符比c要大
                while (!dq.isEmpty() && dq.peekLast() > c && arr[dq.peekLast() - 'a'] > 0) {
                    set.remove(dq.pollLast());
                }
                dq.addLast(c);
                set.add(c);
            }
            arr[c - 'a']--;
        }
        StringBuilder sb = new StringBuilder();
        while (!dq.isEmpty()) sb.append(dq.pollFirst());
        return sb.toString();
    }
}