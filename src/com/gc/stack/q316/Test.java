package com.gc.stack.q316;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 20:49
 */
public class Test {
    public String removeDuplicateLetters(String s) {
        if (s.length() <= 1) return s;
        int[] arr = new int[26];
        for (int i = 0; i < s.length(); i++) {
            arr[s.charAt(i) - 'a']++;
        }
        HashSet<Character> set = new HashSet<>();
        Deque<Character> dq = new LinkedList<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (!set.contains(c)) {
                while (!dq.isEmpty() && dq.peekLast() > c && arr[dq.peekLast() - 'a'] > 0) {
                    set.remove(dq.pollLast());
                }
                set.add(c);
                dq.offerLast(c);
            }
            arr[c - 'a']--;
        }
        StringBuilder sb = new StringBuilder();
        while (!dq.isEmpty()) {
            sb.append(dq.pollFirst());
        }
        return sb.toString();
    }
}