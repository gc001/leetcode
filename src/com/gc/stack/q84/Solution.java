package com.gc.stack.q84;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description: 柱状图中最大的矩形
 * @author: gc
 * @create: 2022-05-01 14:48
 * @refer https://leetcode-cn.com/problems/largest-rectangle-in-histogram/solution/zhao-liang-bian-di-yi-ge-xiao-yu-ta-de-zhi-by-powc/
 * @refer https://leetcode.cn/problems/largest-rectangle-in-histogram/solution/84-by-ikaruga/
 * 前后加0，减少特殊处理
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().largestRectangleArea(new int[]{2, 1, 5, 6, 2, 3}));
    }

    public int largestRectangleArea(int[] arr) {
        Deque<Integer> stack = new LinkedList<>();
        int res = 0;
        int[] heights = new int[arr.length + 2];
        for (int i = 0; i < arr.length; i++) {
            heights[i + 1] = arr[i];
        }
        for (int i = 0; i < heights.length; i++) {
            int cur = heights[i];
            while (!stack.isEmpty() && cur < heights[stack.peek()]) {
                int h = heights[stack.pop()];
                int w = i - stack.peek() - 1;
                res = Math.max(res, w * h);
            }
            stack.push(i);
        }
        return res;
    }
}