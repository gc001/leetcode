package com.gc.stack.q84;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 18:54
 */
public class Test {

    public static void main(String[] args) {

    }

    public int largestRectangleArea(int[] heights) {
        int[] arr = new int[heights.length + 2];
        for (int i = 0; i < heights.length; i++) {
            arr[i + 1] = heights[i];
        }
        Deque<Integer> stack = new LinkedList<>();
        int res = 0;
        for (int i = 0; i < arr.length; i++) {
            while (!stack.isEmpty() && arr[stack.peek()] > arr[i]) {
                int h = arr[stack.pop()];
                int w = i - 1 - stack.peek();
                res = Math.max(res, h * w);
            }
            stack.push(i);
        }
        return res;
    }
}