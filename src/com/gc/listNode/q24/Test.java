package com.gc.listNode.q24;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 21:20
 */
public class Test {
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode node = head.next;
        ListNode after = node.next;
        node.next = null;
        node.next = head;
        head.next = swapPairs(after);
        return node;
    }
}