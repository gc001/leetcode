package com.gc.listNode.q24;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 两两交换链表中的节点
 * @author: gc
 * @create: 2022-04-07 21:52
 */
public class Solution {
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode after = head.next.next;
        head.next.next = null;
        ListNode second = head.next;
        second.next = head;
        head.next = swapPairs(after);
        return second;
    }
}