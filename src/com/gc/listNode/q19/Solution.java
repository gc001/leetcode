package com.gc.listNode.q19;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 删除链表的倒数第 N 个结点
 * @author: gc
 * @create: 2022-04-07 21:40
 */
public class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if (head == null || n <= 0) return null;
        ListNode p1 = head;
        ListNode p2 = head;
        for (int i = 0; i < n; i++) {
            p1 = p1.next;
            if (p1 == null) return head.next;
        }
        while (p1.next != null) {
            p1 = p1.next;
            p2 = p2.next;
        }
        p2.next = p2.next.next;
        return head;
    }
}