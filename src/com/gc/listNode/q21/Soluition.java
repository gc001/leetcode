package com.gc.listNode.q21;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 合并两个有序链表
 * @author: gc
 * @create: 2022-04-05 17:00
 */
public class Soluition {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null) return list2;
        if (list2 == null) return list1;
        ListNode header = new ListNode();
        ListNode cur = header;
        while (list1 != null && list2 != null) {
            if (list1.val < list2.val) {
                cur.next = new ListNode(list1.val);
                list1 = list1.next;
            } else {
                cur.next = new ListNode(list2.val);
                list2 = list2.next;
            }
            cur = cur.next;
        }
        if (list1 != null) cur.next = list1;
        if (list2 != null) cur.next = list2;
        return header.next;
    }
}