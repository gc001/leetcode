package com.gc.listNode.q146;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @program: leetcode
 * @description: LRU缓存
 * @author: gc
 * @create: 2022-04-06 20:59
 */
public class LRUCache {

    Map<Integer, Integer> cache;

    public LRUCache(int capacity) {
        cache = new LinkedHashMap<Integer, Integer>(capacity, 0.75f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
                return capacity < cache.size();
            }
        };
    }

    public int get(int key) {
        return cache.getOrDefault(key, -1);
    }

    public void put(int key, int value) {
        cache.put(key, value);
    }

}