package com.gc.listNode.q146;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 21:31
 */
public class Test {

    static class LRUCache {

        LinkedHashMap<Integer, Integer> cache;

        public LRUCache(int capacity) {
            cache = new LinkedHashMap<Integer, Integer>(capacity, 0.75f, true) {
                @Override
                protected boolean removeEldestEntry(Map.Entry eldest) {
                    return capacity < this.size();
                }
            };
        }

        public int get(int key) {
            return cache.getOrDefault(key,-1);
        }

        public void put(int key, int value) {
            cache.put(key, value);
        }
    }


}