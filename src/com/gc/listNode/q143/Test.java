package com.gc.listNode.q143;

import com.gc.common.CommonUtil;
import com.gc.common.ListNode;

import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 21:53
 */
public class Test {

    public static void main(String[] args) {
        System.out.println(CommonUtil.createListNode(new Integer[]{1, 2, 3}));
    }

    public void reorderList(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        ListNode second = slow.next;
        slow.next = null;
        second = reverse(second);
        ListNode first = head;
        while (first != null && second != null) {
            ListNode n1 = first.next;
            ListNode n2 = second.next;
            first.next = second;
            second.next = n1;
            first = n1;
            second = n2;
        }
    }

    ListNode reverse(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode pre = null;
        ListNode cur = head;
        ListNode lat = head.next;
        while (lat != null) {
            cur.next = pre;
            pre = cur;
            cur = lat;
            lat = lat.next;
        }
        cur.next = pre;
        return cur;
    }
}