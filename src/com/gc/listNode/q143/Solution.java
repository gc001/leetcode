package com.gc.listNode.q143;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 重排链表
 * @author: gc
 * @create: 2022-04-05 21:01
 */
public class Solution {
    public void reorderList(ListNode head) {
        if (head == null || head.next == null) return;
        ListNode fast = head;
        ListNode slow = head;
        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }

        ListNode l1 = head;
        ListNode l2 = slow.next;
        slow.next = null;
        l2 = reverse(l2);
        while (l1 != null && l2 != null) {
            ListNode n1 = l1.next;
            ListNode n2 = l2.next;
            l1.next = l2;
            l2.next = n1;
            l2 = n2;
            l1 = n1;
        }
    }

    ListNode reverse(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode pre = null;
        ListNode cur = head;
        ListNode lat = head.next;
        while (lat != null) {
            cur.next = pre;
            pre = cur;
            cur = lat;
            lat = lat.next;
        }
        cur.next = pre;
        return cur;
    }
}