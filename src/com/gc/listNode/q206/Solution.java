package com.gc.listNode.q206;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 反转链表
 * @author: gc
 * @create: 2022-04-05 17:11
 */
public class Solution {
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode pre = null;
        ListNode cur = head;
        ListNode lat = head.next;
        while (cur.next != null) {
            cur.next = pre;
            pre = cur;
            cur = lat;
            lat = lat.next;
        }
        cur.next = pre;
        return cur;
    }
}