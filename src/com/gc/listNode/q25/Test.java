package com.gc.listNode.q25;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 22:15
 */
public class Test {

    public ListNode reverseKGroup(ListNode head, int k) {
        if (k <= 1 || head == null || head.next == null) return head;
        ListNode node = head;
        for (int i = 0; i < k - 1; i++) {
            node = node.next;
            if (null == node) return head;
        }
        ListNode after = node.next;
        node.next = null;
        ListNode res = reverseList(head);
        head.next = reverseKGroup(after, k);
        return res;
    }

    ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode pre = null;
        ListNode cur = head;
        ListNode lat = head.next;
        while (cur.next != null) {
            cur.next = pre;
            pre = cur;
            cur = lat;
            lat = lat.next;
        }
        cur.next = pre;
        return cur;
    }
}