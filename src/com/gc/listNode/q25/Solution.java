package com.gc.listNode.q25;

import com.gc.common.ListNode;

import java.util.List;

/**
 * @program: leetcode
 * @description: K 个一组翻转链表
 * @author: gc
 * @create: 2022-04-05 20:30
 */
public class Solution {
    public ListNode reverseKGroup(ListNode head, int k) {
        if (head == null || head.next == null || k <= 1) return head;
        ListNode cur = head;
        for (int i = 0; i < k - 1; i++) {
            cur = cur.next;
            if (cur == null) return head;
        }
        ListNode after = cur.next;
        cur.next = null;
        ListNode res = reverseList(head);
        // head 此时在尾部
        head.next = reverseKGroup(after, k);
        return res;
    }

    ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode pre = null;
        ListNode cur = head;
        ListNode lat = head.next;
        while (cur.next != null) {
            cur.next = pre;
            pre = cur;
            cur = lat;
            lat = lat.next;
        }
        cur.next = pre;
        return cur;
    }
}