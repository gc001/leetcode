package com.gc.listNode.q141;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 21:23
 */
public class Test {
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) return false;
        ListNode fast = head;
        ListNode slow = head;
        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
            if (fast == slow) return true;
        }
        return false;
    }
}