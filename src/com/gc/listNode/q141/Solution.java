package com.gc.listNode.q141;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 环形链表
 * @author: gc
 * @create: 2022-04-06 21:49
 */
public class Solution {
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) return false;
        ListNode slow = head;
        ListNode fast = head;
        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) return true;
        }
        return false;
    }
}