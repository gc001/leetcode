package com.gc.listNode.offer.q22;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 22:08
 */
public class Test {
    public ListNode getKthFromEnd(ListNode head, int k) {
        if (k <= 0 || head == null) return head;
        ListNode fast = head;
        ListNode slow = head;
        for (int i = 0; i < k; i++) {
            fast = fast.next;
            if (fast == null) return head;
        }
        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        return slow.next;
    }
}