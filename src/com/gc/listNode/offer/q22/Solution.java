package com.gc.listNode.offer.q22;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 链表中倒数第k个节点
 * @author: gc
 * @create: 2022-04-05 17:34
 */
public class Solution {

    public ListNode getKthFromEnd(ListNode head, int k) {
        ListNode fast = head;
        ListNode slow = head;
        while (k > 0) {
            if (fast.next == null) return head;
            fast = fast.next;
            k--;
        }
        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        return slow.next;
    }

}