package com.gc.listNode.offer.q52;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 两个链表的第一个公共节点
 * @author: gc
 * @create: 2022-04-08 22:35
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode l1, ListNode l2) {
        if (l1 == null || l2 == null) return null;
        int n1 = 0;
        int n2 = 0;
        ListNode h1 = l1;
        ListNode h2 = l2;
        while (l1 != null) {
            n1++;
            l1 = l1.next;
        }
        while (l2 != null) {
            n2++;
            l2 = l2.next;
        }
        int diff = n1 - n2;
        if (diff > 0) {
            while (diff-- > 0)
                h1 = h1.next;
        } else {
            diff = -diff;
            while (diff-- > 0)
                h2 = h2.next;
        }
        while (h1 != h2) {
            h1 = h1.next;
            h2 = h2.next;
        }
        return h1;
    }
}