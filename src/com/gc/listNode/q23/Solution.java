package com.gc.listNode.q23;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 合并K个升序链表
 * @author: gc
 * @create: 2022-04-05 17:05
 */
public class Solution {

    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) return null;
        return mergeKLists(lists, 0, lists.length - 1);
    }

    ListNode mergeKLists(ListNode[] lists, int begin, int end) {
        if (begin > end) return null;
        if (begin == end) return lists[end];
        int mid = (end - begin) / 2 + begin;
        ListNode l1 = mergeKLists(lists, begin, mid);
        ListNode l2 = mergeKLists(lists, mid + 1, end);
        return mergeTwoLists(l1, l2);
    }


    ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null) return list2;
        if (list2 == null) return list1;
        ListNode header = new ListNode();
        ListNode cur = header;
        while (list1 != null && list2 != null) {
            if (list1.val < list2.val) {
                cur.next = new ListNode(list1.val);
                list1 = list1.next;
            } else {
                cur.next = new ListNode(list2.val);
                list2 = list2.next;
            }
            cur = cur.next;
        }
        if (list1 != null) cur.next = list1;
        if (list2 != null) cur.next = list2;
        return header.next;
    }
}