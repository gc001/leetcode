package com.gc.listNode.q92;

import com.gc.common.CommonUtil;
import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description: 反转链表2
 * @author: gc
 * @create: 2022-04-06 21:14
 */
public class Solution {

    public ListNode reverseBetween(ListNode head, int left, int right) {
        if (head == null || left >= right) return head;
        ListNode from = null;
        ListNode to = null;
        ListNode cur = head;
        int cnt = 0;
        while (cur != null) {
            cnt++;
            if (cnt == left - 1) from = cur;
            if (cnt == right + 1) to = cur;
            cur = cur.next;
        }
        if (from == null) {
            cur = head;
        } else {
            cur = from.next;
        }
        ListNode pre = to;
        ListNode lat = cur.next;
        while (lat != to) {
            cur.next = pre;
            pre = cur;
            cur = lat;
            lat = lat.next;
        }
        cur.next = pre;
        if (from == null) return cur;
        from.next = cur;
        return head;
    }

}