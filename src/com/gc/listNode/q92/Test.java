package com.gc.listNode.q92;

import com.gc.common.ListNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 22:21
 */
public class Test {
    public ListNode reverseBetween(ListNode head, int left, int right) {
        if (left > right || head == null || head.next == null) return head;
        int cnt = 0;
        ListNode cur = head;
        ListNode from = null;
        ListNode to = null;
        while (null != cur) {
            cnt++;
            if (cnt == left - 1) {
                from = cur;
            } else if (cnt == right + 1) {
                to = cur;
                break;
            }
            cur = cur.next;
        }
        if(from==null) cur = head;
        else cur = from.next;
        ListNode pre = to;
        ListNode lat = cur.next;
        while (lat != to){
            cur.next = pre;
            pre = cur;
            cur = lat;
            lat = lat.next;
        }
        cur.next = pre;
        if(from == null) return cur;
        from.next = cur;
        return head;
    }
}