package com.gc.tree.q1305;

import com.gc.common.TreeNode;

import java.util.*;

/**
 * @program: leetcode
 * @description: 两棵二叉搜索树中的所有元素
 * @author: gc
 * @create: 2022-05-01 10:26
 */
public class Solution {
    public List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
        List<Integer> list = new ArrayList<>(2000);
        Deque<TreeNode> stack1 = new LinkedList<>();
        Deque<TreeNode> stack2 = new LinkedList<>();
        List<Integer> result = new LinkedList<>();
        while ((!stack1.isEmpty() || root1 != null) && (!stack2.isEmpty() || root2 != null)) {
            while (root1 != null) {
                stack1.push(root1);
                root1 = root1.left;
            }
            while (root2 != null) {
                stack2.push(root2);
                root2 = root2.left;
            }
            if (stack1.peek().val < stack2.peek().val) {
                root1 = stack1.pop();
                result.add(root1.val);
                root1 = root1.right;
            } else {
                root2 = stack2.pop();
                result.add(root2.val);
                root2 = root2.right;
            }
        }
        helper(root1, stack1, result);
        helper(root2, stack2, result);
        return result;
    }

    void helper(TreeNode root, Deque<TreeNode> stack, List<Integer> list) {
        while (root != null || !stack.isEmpty()) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            list.add(root.val);
            root = root.right;
        }
    }
}