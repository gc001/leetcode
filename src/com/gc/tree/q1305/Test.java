package com.gc.tree.q1305;

import com.gc.common.TreeNode;

import java.util.*;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-26 21:49
 */
public class Test {
    public List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
        List<Integer> list = new ArrayList<>();
        Deque<TreeNode> stack1 = new LinkedList<>();
        Deque<TreeNode> stack2 = new LinkedList<>();
        while (root1 != null || !stack1.isEmpty() || root2 != null || !stack2.isEmpty()) {
            while (root1 != null) {
                stack1.push(root1);
                root1 = root1.left;
            }
            while (root2 != null) {
                stack2.push(root2);
                root2 = root2.left;
            }
            if (!stack1.isEmpty() && !stack2.isEmpty()) {
                if (stack1.peek().val < stack2.peek().val) {
                    root1 = stack1.pop();
                    list.add(root1.val);
                    root1 = root1.right;
                } else {
                    root2 = stack2.pop();
                    list.add(root2.val);
                    root2 = root2.right;
                }
            } else if (stack1.isEmpty()) {
                root2 = stack2.pop();
                list.add(root2.val);
                root2 = root2.right;
            } else {
                root1 = stack1.pop();
                list.add(root1.val);
                root1 = root1.right;
            }
        }
        return list;
    }
}