package com.gc.tree.q102;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @program: leetcode
 * @description: 二叉树的层序遍历
 * @author: gc
 * @create: 2022-03-16 21:14
 */
public class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        while (q.size() > 0) {
            List<Integer> sub = new ArrayList<>();
            int cnt = q.size();
            for (int i = 0; i < cnt; i++) {
                TreeNode node = q.poll();
                sub.add(node.val);
                if (node.left != null) {
                    q.add(node.left);
                }
                if (node.right != null) {
                    q.add(node.right);
                }
            }
            list.add(sub);
        }
        return list;
    }
}