package com.gc.tree.q145;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-26 22:30
 */
public class Test {
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        Deque<TreeNode> stack1 = new LinkedList<>();
        Deque<Character> stack2 = new LinkedList<>();
        while (root != null || !stack1.isEmpty()) {
            while (root != null){
                stack1.push(root);
                stack2.push('n');
                root = root.left;
            }
            while (!stack2.isEmpty() && stack2.peek()=='y'){
                stack2.pop();
                list.add(stack1.pop().val);
            }
            while (!stack2.isEmpty() && stack2.peek()=='n'){
                stack2.pop();
                stack2.push('y');
                root = stack1.peek().right;
            }
        }
        return list;
    }
}