package com.gc.tree.q145;

import com.gc.common.TreeNode;

import java.util.*;

/**
 * @program: leetcode
 * @description: 二叉树的后序遍历
 * @author: gc
 * @create: 2022-03-24 21:16
 */
public class Solution {
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        Stack<TreeNode> stack1 = new Stack<>();
        Stack<Character> stack2 = new Stack<>();
        while (root != null || !stack1.isEmpty()) {
            while (root != null) {
                stack1.push(root);
                root = root.left;
                stack2.push('n');
            }

            while (!stack1.isEmpty() && stack2.peek() == 'y') {
                list.add(stack1.pop().val);
                stack2.pop();
            }

            while (!stack1.isEmpty() && stack2.peek() == 'n') {
                root = stack1.peek();
                root = root.right;
                stack2.pop();
                stack2.push('y');
            }
        }
        return list;
    }
}