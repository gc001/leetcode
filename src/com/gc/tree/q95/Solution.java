package com.gc.tree.q95;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 不同的二叉搜索树 II
 * @author: gc
 * @create: 2022-03-26 16:55
 */
class Solution {

    public List<TreeNode> generateTrees(int n) {
        return helper(1, n);
    }

    List<TreeNode> helper(int left, int right) {
        List<TreeNode> list = new ArrayList<>();
        if (left > right) {
            // 如果不add null， 下面的双循环不会执行
            list.add(null);
            return list;
        }
        for (int i = left; i <= right; i++) {
            List<TreeNode> l = helper(left, i - 1);
            List<TreeNode> r = helper(i + 1, right);
            for (TreeNode ln : l) {
                for (TreeNode rn : r) {
                    TreeNode root = new TreeNode(i);
                    root.left = ln;
                    root.right = rn;
                    list.add(root);
                }
            }
        }
        return list;
    }
}