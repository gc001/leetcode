package com.gc.tree.q95;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-23 22:05
 */
public class Test {
    public List<TreeNode> generateTrees(int n) {
        return helper(1, n);
    }

    List<TreeNode> helper(int left, int right) {
        List<TreeNode> list = new ArrayList<>();
        if (right < left) {
            list.add(null);
            return list;
        }
        for (int i = left; i <= right; i++) {
            List<TreeNode> ll = helper(left, i - 1);
            List<TreeNode> rl = helper(i + 1, right);
            for (TreeNode ln : ll) {
                for (TreeNode rn : rl) {
                    TreeNode root = new TreeNode(i);
                    root.left = ln;
                    root.right = rn;
                    list.add(root);
                }
            }
        }
        return list;
    }
}