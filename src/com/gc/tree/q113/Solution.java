package com.gc.tree.q113;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 路径总和 II
 * @author: gc
 * @create: 2022-03-21 21:30
 */
public class Solution {
    List<List<Integer>> list = new ArrayList<>();
    List<Integer> sub = new ArrayList<>();

    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        helper(root, targetSum);
        return list;
    }

    void helper(TreeNode root, int target) {
        if (root == null) return;
        target -= root.val;
        sub.add(root.val);
        if (target == 0 && root.left == null && root.right == null) {
            list.add(new ArrayList<>(sub));
        }
        helper(root.left, target);
        helper(root.right, target);
        sub.remove(sub.size() - 1);
    }
}