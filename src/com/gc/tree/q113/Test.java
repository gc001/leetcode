package com.gc.tree.q113;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-24 21:45
 */
public class Test {
    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<List<Integer>> list = new ArrayList<>();
        helper(root, targetSum, list, new ArrayList<>());
        return list;
    }

    void helper(TreeNode root, int target, List<List<Integer>> list, List<Integer> sub) {
        if (root == null) return;
        target -= root.val;
        sub.add(root.val);
        if (root.left == null && root.right == null && target == 0) {
            list.add(new ArrayList<>(sub));
        }
        helper(root.left, target, list, sub);
        helper(root.right, target, list, sub);
        sub.remove(sub.size() - 1);
    }
}