package com.gc.tree.q235;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 二叉搜索树的最近公共祖先
 * @author: gc
 * @create: 2022-03-24 21:06
 */
public class Solution {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return null;
        if ((root.val - p.val) * (root.val - q.val) <= 0) return root;
        if (root.val > p.val && root.val > q.val) return lowestCommonAncestor(root.left, p, q);
        return lowestCommonAncestor(root.right, p, q);
    }
}
