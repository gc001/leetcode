package com.gc.tree.q257;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 二叉树的所有路径
 * @author: gc
 * @create: 2022-03-20 12:12
 */
public class Solution {
    List<String> list = new ArrayList<>();

    public List<String> binaryTreePaths(TreeNode root) {
        helper(root, "");
        return list;
    }

    public void helper(TreeNode node, String sub) {
        if (node == null) return;
        sub += node.val + "->";
        if (node.left == null && node.right == null) {
            list.add(sub.substring(0, sub.length() - 2));
            return;
        }
        helper(node.left, sub);
        helper(node.right, sub);
    }
}