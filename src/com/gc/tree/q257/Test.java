package com.gc.tree.q257;

import com.gc.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-25 22:12
 */
public class Test {

    public List<String> binaryTreePaths(TreeNode root) {
        List<String> list = new ArrayList<>();
        helper(root, "", list);
        return list;
    }

    void helper(TreeNode root, String sub, List<String> list) {
        if (root == null) return;
        sub += "->" + root.val;
        if (root.left == null && root.right == null) {
            list.add(sub.substring(2));
            return;
        }
        helper(root.left, sub, list);
        helper(root.right, sub, list);
    }

}