package com.gc.tree.q98;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-23 22:16
 */
public class Test {
    private long pre = Long.MIN_VALUE;
    public boolean isValidBST(TreeNode root) {
        if(root == null) return true;
        boolean left = isValidBST(root.left);
        if(left) {
            if(pre < root.val) {
                pre = root.val;
                return isValidBST(root.right);
            }
        }
        return false;
    }
}