package com.gc.tree.q98;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 验证二叉搜索树
 * @author: gc
 * @create: 2022-03-20 12:29
 */
public class Solution {
    long pre = Long.MIN_VALUE;

    public boolean isValidBST(TreeNode root) {
        if (root == null) return true;
        boolean left = isValidBST(root.left);
        if (left && pre < root.val) {
            pre = root.val;
            return isValidBST(root.right);
        }
        return false;
    }
}