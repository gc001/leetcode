package com.gc.tree.offer.q54;

import com.gc.common.CommonUtil;
import com.gc.common.ListNode;
import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 18:57
 */
public class Test {

    int res;
    int n;

    public int kthLargest(TreeNode root, int k) {
        n = k;
        helper(root);
        return res;
    }

    void helper(TreeNode root) {
        if (root == null) return;
        helper(root.right);
        n--;
        if (n == 0) {
            res = root.val;
        } else if (n > 0) {
            helper(root.left);
        }
    }

    public static void main(String[] args) {
        TreeNode treeNode = CommonUtil.createTreeNode(new Integer[]{3, 1, 4, null, 2});
        new Test().kthLargest(treeNode, 1);
    }
}