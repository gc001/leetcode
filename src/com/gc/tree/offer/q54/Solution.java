package com.gc.tree.offer.q54;

import com.gc.common.CommonUtil;
import com.gc.common.TreeNode;

/**
 *  二叉搜索树的第k大节点
 */
public class Solution {
    int n = -1;
    TreeNode node = null;

    public int kthLargest(TreeNode root, int k) {
        n = k;
        helper(root);
        return node.val;
    }

    void helper(TreeNode root) {
        if (root == null) return;
        helper(root.right);
        n--;
        if (n == 0) {
            node = root;
            return;
        }
        if (n > 0) {
            helper(root.left);
        }
    }
    public static void main(String[] args) {
        TreeNode treeNode = CommonUtil.createTreeNode(new Integer[]{3, 1, 4, null, 2});
        new Solution().kthLargest(treeNode, 1);
    }
}
