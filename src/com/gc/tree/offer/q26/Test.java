package com.gc.tree.offer.q26;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 18:17
 */
public class Test {
    public boolean isSubStructure(TreeNode r1, TreeNode r2) {
        if (r1 == null || r2 == null) {
            return false;
        }
        return helper(r1, r2) || isSubStructure(r1.left, r2) || isSubStructure(r1.right, r2);
    }

    boolean helper(TreeNode r1, TreeNode r2) {
        if (r2 == null) return true;
        if (r1 == null || r1.val != r2.val) return false;
        return helper(r1.left, r2.left) && helper(r1.right, r2.right);
    }
}