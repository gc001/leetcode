package com.gc.tree.offer.q26;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 树的子结构
 * @author: gc
 * @create: 2022-03-17 21:18
 */
public class Solution {
    public boolean isSubStructure(TreeNode t1, TreeNode t2) {
        if (t1 == null || t2 == null) {
            return false;
        }
        boolean res = false;
        res = helper(t1, t2);
        if (!res) {
            res = isSubStructure(t1.left, t2);
        }
        if (!res) {
            res = isSubStructure(t1.right, t2);
        }
        return res;
    }

    public boolean helper(TreeNode t1, TreeNode t2) {
        if (t2 == null) {
            return true;
        }
        if (t1 == null) {
            return false;
        }
        if (t1.val != t2.val) {
            return false;
        }
        return helper(t1.left, t2.left) && helper(t1.right, t2.right);
    }

}