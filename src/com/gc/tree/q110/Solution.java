package com.gc.tree.q110;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 平衡二叉树
 * @author: gc
 * @create: 2022-03-25 22:28
 */
class Solution {
    public boolean isBalanced(TreeNode root) {
        return helper(root) >= 0;
    }

    int helper(TreeNode root) {
        if (root == null) return 0;
        int left = helper(root.left);
        int right = helper(root.right);
        if (left >= 0 && right >= 0 && Math.abs(left - right) <= 1) {
            return Math.max(left, right) + 1;
        }
        return -1;
    }
}