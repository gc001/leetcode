package com.gc.tree.q236;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 二叉树的最近公共祖先
 * @author: gc
 * @create: 2022-03-18 21:58
 */
public class Solution {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return null;
        if (root.val == p.val) return root;
        if (root.val == q.val) return root;
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if (left != null && right != null) return root;
        if (left != null) return left;
        return right;
    }
}