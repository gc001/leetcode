package com.gc.tree.q105;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 从前序与中序遍历序列构造二叉树
 * @author: gc
 * @create: 2022-03-22 21:43
 */
public class Solution {

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return helper(0, preorder.length - 1, preorder, 0, inorder.length - 1, inorder);
    }

    TreeNode helper(int a, int b, int[] preorder, int x, int y, int[] inorder) {
        if (a > b || x > y) return null;
        int i = x;
        for (; i <= b; i++) {
            if (preorder[a] == inorder[i]) {
                break;
            }
        }
        TreeNode root = new TreeNode(preorder[a]);
        root.left = helper(a + 1, a + i - x, preorder, x, i - 1, inorder);
        root.right = helper(a + 1 + i - x, b, preorder, i + 1, b, inorder);
        return root;
    }
}