package com.gc.tree.q105;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 19:25
 */
public class Test {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return helper(preorder, 0, preorder.length - 1, inorder, 0, preorder.length - 1);
    }

    TreeNode helper(int[] preorder, int i, int j, int[] inorder, int m, int n) {
        if (i > j || m > n) return null;
        int rootVal = preorder[i];
        int k = m;
        for (; k <= n; k++) {
            if (inorder[k] == rootVal) {
                break;
            }
        }
        TreeNode root = new TreeNode(rootVal);
        root.left = helper(preorder, i + 1, i + k - m, inorder, m, k - 1);
        root.right = helper(preorder, i + k - m + 1, j, inorder, k + 1, n);
        return root;
    }
}