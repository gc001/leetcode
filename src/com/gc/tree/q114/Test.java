package com.gc.tree.q114;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-24 21:49
 */
public class Test {
    public void flatten(TreeNode root) {
        if (root == null) return;
        flatten(root.left);
        flatten(root.right);
        TreeNode r = root.right;
        TreeNode l = root.left;
        root.left = null;
        root.right = l;
        while (root.right != null) {
            root = root.right;
        }
        root.right = r;
    }
}