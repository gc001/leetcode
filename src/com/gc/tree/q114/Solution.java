package com.gc.tree.q114;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 二叉树展开为链表
 * @author: gc
 * @create: 2022-03-26 16:48
 */
public class Solution {
    public void flatten(TreeNode root) {
        if (root == null) return;
        flatten(root.left);
        flatten(root.right);
        TreeNode node = root.right;
        root.right = root.left;
        root.left = null;
        while (root.right != null) {
            root = root.right;
        }
        root.right = node;
    }
}