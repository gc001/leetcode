package com.gc.tree.q101;

import com.gc.common.TreeNode;
import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * @program: leetcode
 * @description: 对称二叉树
 * @author: gc
 * @create: 2022-03-17 21:46
 */
public class Solution {

    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return helper(root.left, root.right);
    }

    public boolean helper(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return true;
        }
        if (t1 == null || t2 == null || t1.val != t2.val) {
            return false;
        }
        return helper(t1.left, t2.right) && helper(t1.right, t2.left);
    }

}