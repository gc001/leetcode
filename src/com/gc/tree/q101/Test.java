package com.gc.tree.q101;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-23 22:20
 */
public class Test {
    public boolean isSymmetric(TreeNode root) {
        if (root == null) return true;
        return helper(root.left, root.right);
    }

    boolean helper(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) return true;
        if (t1 == null || t2 == null || t1.val != t2.val) return false;
        return helper(t1.left, t2.right) && helper(t1.right, t2.left);
    }
}