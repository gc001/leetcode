package com.gc.tree.q112;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-24 21:38
 */
public class Test {
    public boolean hasPathSum(TreeNode root, int target) {
        if (root == null) return false;
        target -= root.val;
        if (root.left == null && root.right == null && target == 0) return true;
        return hasPathSum(root.left, target) || hasPathSum(root.right, target);
    }


}