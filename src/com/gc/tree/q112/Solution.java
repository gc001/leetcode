package com.gc.tree.q112;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 路径总和
 * @author: gc
 * @create: 2022-03-21 21:30
 */
public class Solution {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) return false;
        targetSum -= root.val;
        if (targetSum == 0 && root.left == null && root.right == null) return true;
        return hasPathSum(root.left, targetSum) || hasPathSum(root.right, targetSum);
    }
}