package com.gc.tree.q297;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 二叉树的序列化与反序列化
 * @author: gc
 * @create: 2022-03-23 21:48
 */
public class Codec {


    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        // & 表示 null
        if(root == null) return "&,";
        StringBuffer sb = new StringBuffer();
        sb.append(root.val).append(",");
        sb.append(serialize(root.left));
        sb.append(serialize(root.right));
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String[] datas = data.split(",");
        return deserHelper(datas);
    }

    int idx = -1;
    public TreeNode deserHelper(String[] datas){
        idx++;
        if(idx>=datas.length){
            return null;
        }
        if(datas[idx].equals("&")){
            return null;
        }
        TreeNode root = new TreeNode(Integer.valueOf(datas[idx]));
        root.left = deserHelper(datas);
        root.right = deserHelper(datas);
        return root;

    }
}

// Your Codec object will be instantiated and called as such:
// Codec ser = new Codec();
// Codec deser = new Codec();
// TreeNode ans = deser.deserialize(ser.serialize(root));