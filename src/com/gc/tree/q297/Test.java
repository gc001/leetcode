package com.gc.tree.q297;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 19:14
 */
public class Test {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (root == null) return "&,";
        StringBuilder sb = new StringBuilder();
        String left = serialize(root.left);
        String right = serialize(root.right);
        sb.append(root.val).append(",").append(left).append(right);
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String[] split = data.split(",");
        return deseHelper(split);
    }

    int idx = -1;

    TreeNode deseHelper(String[] datas) {
        idx++;
        if (idx >= datas.length || datas[idx].equals("&")) return null;
        TreeNode root = new TreeNode(Integer.parseInt(datas[idx]));
        root.left = deseHelper(datas);
        root.right = deseHelper(datas);
        return root;
    }


}