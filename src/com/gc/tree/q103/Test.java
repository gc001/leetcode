package com.gc.tree.q103;

import com.gc.common.TreeNode;

import java.util.*;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-23 22:32
 */
public class Test {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        boolean f = true;
        while (!q.isEmpty()) {
            int size = q.size();
            Integer[] arr = new Integer[size];
            for (int i = 0; i < size; i++) {
                root = q.poll();
                arr[f ? i : size - i - 1] = root.val;
                if (root.left != null) q.offer(root.left);
                if (root.right != null) q.offer(root.right);
            }
            list.add(Arrays.asList(arr));
            f = !f;
        }
        return list;
    }
}