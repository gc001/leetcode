package com.gc.tree.q103;

import com.gc.common.TreeNode;
import sun.reflect.generics.tree.Tree;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: leetcode
 * @description: 二叉树的锯齿形层序遍历
 * @author: gc
 * @create: 2022-03-20 10:46
 */
public class Solution {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> list = new ArrayList<>();
        if (root == null) return list;
        boolean flag = true;
        Deque<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while (q.size() > 0) {
            int cnt = q.size();
            Integer[] array = new Integer[cnt];
            for (int i = 0; i < cnt; i++) {
                TreeNode node = q.poll();
                array[flag ? i : array.length - i - 1] = node.val;
                if (node.left != null) {
                    q.offer(node.left);
                }
                if (node.right != null) {
                    q.offer(node.right);
                }
            }
            list.add(Arrays.asList(array));
            flag = !flag;
        }
        return list;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(10);
        System.out.println(list.size());
        list.add(1);
        System.out.println(list.size());
    }

}