package com.gc.tree.q617;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 合并二叉树
 * @author: gc
 * @create: 2022-03-20 12:04
 */
public class Solution {
    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null) return t2;
        if (t2 == null) return t1;
        TreeNode root = new TreeNode();
        root.val = t1.val + t2.val;
        root.left = mergeTrees(t1.left, t2.left);
        root.right = mergeTrees(t1.right, t2.right);
        return root;
    }
}