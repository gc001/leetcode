package com.gc.tree.q124;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:  二叉树中的最大路径和
 * @author: gc
 * @create: 2022-03-16 21:24
 */
public class Solution {
    int res = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {
        if (root == null) {
            return 0;
        }
        helper(root);
        return res;
    }

    public int helper(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = Math.max(helper(root.left), 0);
        int right = Math.max(helper(root.right), 0);
        res = Math.max(res, root.val + left + right);
        return root.val + Math.max(left, right);
    }
}