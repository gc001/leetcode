package com.gc.tree.q226;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 翻转二叉树
 * @author: gc
 * @create: 2022-03-17 21:09
 */
class Solution {
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return root;
        }
        invertTree(root.left);
        invertTree(root.right);
        TreeNode node = root.left;
        root.left = root.right;
        root.right = node;
        return root;
    }
}