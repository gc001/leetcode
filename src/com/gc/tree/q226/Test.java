package com.gc.tree.q226;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-24 21:58
 */
public class Test {
    public TreeNode invertTree(TreeNode root) {
        if (root == null) return root;
        invertTree(root.left);
        invertTree(root.right);
        TreeNode l = root.left;
        root.left = root.right;
        root.right = l;
        return root;
    }
}