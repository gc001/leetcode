package com.gc.tree.q99;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 恢复二叉搜索树
 * @author: gc todo
 * @create: 2022-05-24 21:18
 */
public class Solution {
    private TreeNode first;
    private TreeNode second;
    private boolean f = false;

    public void recoverTree(TreeNode root) {
        helper(root);
    }

    void helper(TreeNode root) {
        if (root == null || f) return;
        helper(root.left);
        if (first == null) {
            first = root;
        } else if (second == null) {
            second = root;
        } else {
            first = second;
            second = root;
        }
        if (second != null) {
            if (first.val > second.val) {
                int t = first.val;
                first.val = second.val;
                second.val = t;
                f = true;
            }
        }
        helper(root.right);
    }
}