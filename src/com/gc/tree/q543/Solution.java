package com.gc.tree.q543;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 二叉树的直径
 * @author: gc
 * @create: 2022-03-16 21:38
 */
public class Solution {

    int res = 0;

    public int diameterOfBinaryTree(TreeNode root) {
        helper(root);
        return res;
    }

    public int helper(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = helper(root.left);
        int right = helper(root.right);
        res = Math.max(res, left + right);
        return Math.max(left, right) + 1;
    }
}