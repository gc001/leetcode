package com.gc.binarySearch.offer.q53;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 10:54
 */
public class Test {
    public int missingNumber(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (r - l) / 2 + l;
            if (nums[m] <= m) l = m + 1;
            else r = m - 1;
        }
        return r == -1 ? 0 : nums[r] + 1;
    }
}