package com.gc.binarySearch.offer.q53;

/**
 * @program: leetcode
 * @description: 剑指 Offer 53 - II. 0～n-1中缺失的数字
 * @author: gc
 * @create: 2022-05-13 21:08
 */
public class Solution {
    public int missingNumber(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (r - l) / 2 + l;
            // 注意相等的情况 看test
            if (nums[m] > m) r = m - 1;
            else l = m + 1;
        }
        return r == -1 ? 0 : nums[r] + 1;
    }
}