package com.gc.binarySearch.q33;

/**
 * @program: leetcode
 * @description: 搜索旋转排序数组
 * @author: gc
 * @create: 2022-05-11 23:50
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().search(new int[]{4, 5, 6, 7, 0, 1, 2}, 6));
    }

    public int search(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (r - l) / 2 + l;
            if (nums[m] == target) return m;
            // 右边有序
            if (nums[m] < nums[r]) {
                // 位于右边有序区间内部 ，注意区间内则应该是闭区间
                if (nums[m] < target && target <= nums[r]) l = m + 1;
                    // 位于左侧区间内（可能有序也可能无序）
                else r = m - 1;
            }
            // 左边有序
            else {
                // 位于左边有序区间内
                if (nums[m] > target && target >= nums[l]) r = m - 1;
                    // 位于右侧区间内（可能有序也可能无序）
                else l = m + 1;
            }
        }
        return -1;
    }
}