package com.gc.binarySearch.q33;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 11:25
 */
public class Test {
    public int search(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (r - l) / 2 + l;
            if (nums[m] == target) return m;
            // 右边有序
            if (nums[m] < nums[r]) {
                // target位于有序部分
                if (target >= nums[m] && target <= nums[r]) l = m + 1;
                else r = m - 1;
            }
            // 左边有序
            else {
                if (target <= nums[m] && target >= nums[l]) r = m - 1;
                else l = m + 1;
            }
        }
        return -1;
    }

}