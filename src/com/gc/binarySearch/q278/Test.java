package com.gc.binarySearch.q278;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 11:11
 */
public abstract class Test {

    public int firstBadVersion(int n) {
        int l = 1;
        int r = n;
        while (l <= r) {
            int mid = (r - l) / 2 + l;
            if (!isBadVersion(mid)) l = mid + 1;
            else r = mid - 1;
        }
        return l;
    }

    abstract boolean isBadVersion(int version);

}