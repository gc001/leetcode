package com.gc.binarySearch.q278;

/**
 * @program: leetcode
 * @description: 第一个错误的版本
 * @author: gc
 * @create: 2022-05-10 21:26
 */
public abstract class Solution {
    public int firstBadVersion(int n) {
        int left = 1;
        int right = n;
        while (left <= right) {
            int mid = (right - left) / 2 + left;
            if (isBadVersion(mid)) right = mid - 1;
            else left = mid + 1;
        }
        return left;
    }

    abstract boolean isBadVersion(int version);
}