package com.gc.binarySearch.q69;

/**
 * @program: leetcode
 * @description: x 的平方根
 * @author: gc
 * @create: 2022-05-09 21:10
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().mySqrt(9));
    }

    public int mySqrt(int x) {
        if(x==0) return 0;
        int left = 1;
        int right = x;
        while (left <= right) {
            int mid = (right - left) / 2 + left;
            if (x / mid == mid) return mid;
            if (mid > x / mid) right = mid - 1;
            else left = mid + 1;
        }
        return right;
    }
}