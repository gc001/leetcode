package com.gc.binarySearch.q69;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 10:37
 */
public class Test {
    public int mySqrt(int x) {
        int l = 1;
        int r = x;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if (x / mid == mid) return mid;
            if (x / mid > mid) l = mid + 1;
            else r = mid - 1;
        }
        return r;
    }
}