package com.gc.binarySearch.q35;

/**
 * @program: leetcode
 * @description: 搜索插入位置
 * @author: gc
 * @create: 2022-05-10 21:18
 */
public class Solution {
    public int searchInsert(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            int mid = (right - left) / 2 + left;
            if (nums[mid] >= target) right = mid - 1;
            else left = mid + 1;
        }
        return right + 1;
    }
}