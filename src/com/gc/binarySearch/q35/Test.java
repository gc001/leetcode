package com.gc.binarySearch.q35;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 10:35
 */
public class Test {
    public int searchInsert(int[] arr, int target) {
        int l = 0;
        int r = arr.length - 1;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if (arr[mid] == target) return mid;
            if (arr[mid] > target) r = mid - 1;
            else l = mid + 1;
        }
        return r + 1;
    }
}