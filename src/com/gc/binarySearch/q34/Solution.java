package com.gc.binarySearch.q34;

/**
 * @program: leetcode
 * @description: 在排序数组中查找元素的第一个和最后一个位置
 * @author: gc
 * @create: 2022-05-11 23:28
 */
public class Solution {
    public int[] searchRange(int[] nums, int target) {
        if (null == nums || nums.length == 0) return new int[]{-1, -1};
        int l = helper(nums, target);
        if (l == nums.length || nums[l] != target) return new int[]{-1, -1};
        int r = helper(nums, target + 1);
        return new int[]{l, r - 1};
    }

    private int helper(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (r - l) / 2 + l;
            if (nums[m] >= target) r = m - 1;
            else l = m + 1;
        }
        return l;
    }
}