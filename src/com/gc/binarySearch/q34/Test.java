package com.gc.binarySearch.q34;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 11:37
 */
public class Test {
    public int[] searchRange(int[] nums, int target) {
        if (null == nums || nums.length == 0) return new int[]{-1, -1};
        int l = helper(nums, target);
        if (l == nums.length || nums[l] != target) return new int[]{-1, -1};
        return new int[]{l, helper(nums, target + 1) - 1};
    }

    int helper(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (nums[m] >= target) r = m - 1;
            else l = m + 1;
        }
        return l;
    }
}