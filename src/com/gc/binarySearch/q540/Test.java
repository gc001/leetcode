package com.gc.binarySearch.q540;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 11:48
 */
public class Test {
    public int singleNonDuplicate(int[] nums) {
        if (nums.length == 1) return nums[0];
        int l = 0, r = nums.length - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (m % 2 == 0) {
                if (m == nums.length - 1) return nums[m];
                if (nums[m] == nums[m + 1]) l = m + 1;
                else r = m - 1;
            } else {
                if (m == 0) return nums[0];
                if (nums[m] == nums[m - 1]) l = m + 1;
                else r = m - 1;
            }
        }
        return nums[l];
    }
}