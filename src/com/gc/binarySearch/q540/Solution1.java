package com.gc.binarySearch.q540;

/**
 * @program: leetcode
 * @description: 有序数组中的单一元素
 * @author: gc
 * @create: 2022-05-13 23:06
 * 利用按位异或的性质，可以得到 \textit{mid}mid 和相邻的数之间的如下关系，其中 \oplus⊕ 是按位异或运算符：
 * <p>
 * 当 \textit{mid}mid 是偶数时，\textit{mid} + 1 = \textit{mid} \oplus 1mid+1=mid⊕1；
 * <p>
 * 当 \textit{mid}mid 是奇数时，\textit{mid} - 1 = \textit{mid} \oplus 1mid−1=mid⊕1。
 * <p>
 * 作者：LeetCode-Solution
 * 链接：https://leetcode.cn/problems/single-element-in-a-sorted-array/solution/you-xu-shu-zu-zhong-de-dan-yi-yuan-su-by-y8gh/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Solution1 {

    public static void main(String[] args) {
        System.out.println(new Solution1().singleNonDuplicate(new int[]{1, 1, 2}));
    }

    public int singleNonDuplicate(int[] nums) {
        if (nums.length == 1) return nums[0];
        int l = 0;
        int r = nums.length - 1;
        int m;
        while (l <= r) {
            m = (r - l) / 2 + l;
            if (m == nums.length - 1) return nums[m];
            if (nums[m] == nums[m ^ 1]) l = m + 1;
            else r = m - 1;
        }
        return nums[l];
    }
}