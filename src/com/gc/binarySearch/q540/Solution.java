package com.gc.binarySearch.q540;

/**
 * @program: leetcode
 * @description: 有序数组中的单一元素
 * @author: gc
 * @create: 2022-05-13 23:06
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().singleNonDuplicate(new int[]{1, 1, 2}));
    }

    public int singleNonDuplicate(int[] nums) {
        if (nums.length == 1) return nums[0];
        int l = 0;
        int r = nums.length - 1;
        int m;
        while (l <= r) {
            m = (r - l) / 2 + l;
            // 如果m是偶数，就和右边比；是奇数就和左边比
            if (m % 2 == 0) {
                // 比到最后一个，则前面的必然正确
                if (m + 1 == nums.length) return nums[m];
                // 如果和右边的一致，因为前面共有偶数个数，则答案必然在后面
                if (nums[m] == nums[m + 1]) l = m + 1;
                // 若若和右边的不一致，因为前面共有偶数个数，则答案必然在前面
                else r = m - 1;
            } else {
                if (m == 0) return nums[m];
                if (nums[m] == nums[m - 1]) l = m + 1;
                else r = m - 1;
            }
        }
        return nums[l];
    }
}