package com.gc.binarySearch.q704;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 10:19
 */
public class Test {
    public int search(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if (nums[mid] == target) return mid;
            if (nums[mid] > target) r = mid - 1;
            else l = mid + 1;
        }
        return -1;
    }
}