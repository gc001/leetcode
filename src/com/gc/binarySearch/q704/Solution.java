package com.gc.binarySearch.q704;

/**
 * @program: leetcode
 * @description: 二分查找
 * @author: gc
 * @create: 2022-05-09 20:54
 */
public class Solution {
    public int search(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            int mid = (right - left) / 2 + left;
            if (nums[mid] == target) return mid;
            if (nums[mid] > target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return -1;
    }
}