package com.gc.binarySearch.q81;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 12:02
 */
public class Test {
    public boolean search(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            while (l < r && nums[l] == nums[l + 1]) l++;
            while (l < r && nums[r] == nums[r - 1]) r--;
            int m = (r - l) / 2 + l;
            if (nums[m] == target) return true;
            if (nums[m] < nums[r]) {
                if (nums[m] < target && target <= nums[r]) l = m + 1;
                else r = m - 1;
            } else {
                if (nums[m] > target && target >= nums[l]) r = m - 1;
                else l = m + 1;
            }
        }
        return false;
    }
}