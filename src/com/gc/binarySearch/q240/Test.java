package com.gc.binarySearch.q240;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 10:50
 */
public class Test {
    public boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length;
        int n = matrix[0].length;
        int i = 0, j = n - 1;
        while (i >= 0 && i < m && j >= 0 && j < n) {
            if (matrix[i][j] > target) j--;
            else if (matrix[i][j] < target) i++;
            else return true;
        }
        return false;
    }
}