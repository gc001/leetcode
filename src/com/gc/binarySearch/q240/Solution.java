package com.gc.binarySearch.q240;

/**
 * @program: leetcode
 * @description: 搜索二维矩阵 II
 * @author: gc
 * @create: 2022-05-14 16:12
 */
public class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        int i = 0;
        int j = matrix[0].length - 1;
        while (i >= 0 && i < matrix.length && j >= 0 && j < matrix[0].length) {
            if (matrix[i][j] == target) return true;
            if (matrix[i][j] > target) j--;
            else i++;
        }
        return false;
    }
}