package com.gc.binarySearch.q4;

/**
 * @program: leetcode
 * @description: 寻找两个正序数组的中位数
 * @author: gc
 * @create: 2022-05-14 14:51
 */
public class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int l1 = nums1.length;
        int l2 = nums2.length;
        int m1 = (l1 + l2 + 1) / 2;
        int m2 = (l1 + l2 + 2) / 2;
        if (m1 == m2) return helper(nums1, 0, nums2, 0, m1);
        return 0.5 * (helper(nums1, 0, nums2, 0, m1) + helper(nums1, 0, nums2, 0, m2));
    }

    /**
     * @param arr1 arr1
     * @param i    arr1 起始位置
     * @param arr2 arr2
     * @param j    arr2 起始位置
     * @param k    寻找第k个数
     * @return 第k个数
     */
    int helper(int[] arr1, int i, int[] arr2, int j, int k) {
        //  k-1: 比如从第0个起的第三位数字，下标是0+3-1=2
        if (i >= arr1.length) return arr2[j + k - 1];
        if (j >= arr2.length) return arr1[i + k - 1];
        if (k == 1) return Math.min(arr1[i], arr2[j]);
        /* 这里将v1和v2设置成最大值，是因为当数组中不存在第k/2个数的时候，也就是长度不够了，
        要寻找的值必然在另一个数组，因为值大，所以会将另一个数组的前k/2个值淘汰掉*/
        int midV1 = Integer.MAX_VALUE;
        int midV2 = Integer.MAX_VALUE;
        if (i + k / 2 - 1 < arr1.length) midV1 = arr1[i + k / 2 - 1];
        if (j + k / 2 - 1 < arr2.length) midV2 = arr2[j + k / 2 - 1];
        // 淘汰掉arr1 从i开始的前k/2个数
        if (midV1 < midV2) return helper(arr1, i + k / 2, arr2, j, k - k / 2);
        else return helper(arr1, i, arr2, j + k / 2, k - k / 2);
    }
}