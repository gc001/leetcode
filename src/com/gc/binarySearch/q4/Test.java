package com.gc.binarySearch.q4;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 19:22
 */
public class Test {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int l1 = nums1.length;
        int l2 = nums2.length;
        int m1 = (l1 + l2 + 1) / 2;
        int m2 = (l1 + l2 + 2) / 2;
        if (m1 == m2) return 1.0 * helper(nums1, 0, nums2, 0, m1);
        return 0.5 * (helper(nums1, 0, nums2, 0, m1) + helper(nums1, 0, nums2, 0, m2));
    }

    int helper(int[] nums1, int i, int[] nums2, int j, int k) {
        if (i >= nums1.length) return nums2[j + k - 1];
        if (j >= nums2.length) return nums1[i + k - 1];
        if (k == 1) return Math.min(nums1[i], nums2[j]);
        int v1 = Integer.MAX_VALUE;
        int v2 = Integer.MAX_VALUE;
        if (i + k / 2 - 1 < nums1.length) v1 = nums1[i + k / 2 - 1];
        if (j + k / 2 - 1 < nums2.length) v2 = nums2[j + k / 2 - 1];
        if (v1 > v2) {
            return helper(nums1, i, nums2, j + k / 2, k - k / 2);
        } else {
            return helper(nums1, i + k / 2, nums2, j, k - k / 2);
        }
    }


}