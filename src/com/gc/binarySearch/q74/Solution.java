package com.gc.binarySearch.q74;

/**
 * @program: leetcode
 * @description: 搜索二维矩阵
 * @author: gc
 * @create: 2022-05-14 16:29
 */
public class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length;
        int n = matrix[0].length;
        int l = 0, r = m * n - 1;
        while (l <= r) {
            int mid = (r - l) / 2 + l;
            int cur = matrix[mid / n][mid % n];
            if (cur == target) return true;
            if (cur > target) r = mid - 1;
            else l = mid + 1;
        }
        return false;
    }
}
