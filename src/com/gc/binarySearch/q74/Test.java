package com.gc.binarySearch.q74;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 10:45
 */
public class Test {
    public boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length;
        int n = matrix[0].length;
        int l = 0;
        int r = m * n - 1;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            int x = mid / n;
            int y = mid % n;
            if (matrix[x][y] > target) r = mid - 1;
            else if (matrix[x][y] < target) l = mid + 1;
            else return true;
        }
        return false;
    }
}