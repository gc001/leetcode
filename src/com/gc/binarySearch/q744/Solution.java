package com.gc.binarySearch.q744;

/**
 * @program: leetcode
 * @description: 寻找比目标字母大的最小字母
 * @author: gc
 * @create: 2022-05-09 23:19
 */
public class Solution {
    public char nextGreatestLetter(char[] letters, char target) {
        int left = 0;
        int right = letters.length - 1;
        while (left <= right) {
            int mid = (right - left) / 2 + left;
            if (letters[mid] > target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return left >= letters.length ? letters[0] : letters[left];
    }
}