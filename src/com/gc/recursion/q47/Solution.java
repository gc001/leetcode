package com.gc.recursion.q47;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 全排列 II
 * @author: gc
 * @create: 2022-04-03 15:30
 */
public class Solution {

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        helper(nums, 0, list);
        return list;
    }

    void helper(int[] nums, int idx, List<List<Integer>> list) {
        if (idx == nums.length) {
            List<Integer> sub = new ArrayList<>();
            for (int num : nums) {
                sub.add(num);
            }
            list.add(new ArrayList<>(sub));
            return;
        }
        for (int i = idx; i < nums.length; i++) {
            if (check(nums, idx, i)) {
                swap(nums, i, idx);
                helper(nums, idx + 1, list);
                swap(nums, i, idx);
            }
        }

    }

    boolean check(int[] arr, int start, int end) {
        for (int i = start; i < end; i++) {
            if (arr[i] == arr[end])
                return false;
        }
        return true;
    }

    void swap(int[] arr, int i, int j) {
        int t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }
}