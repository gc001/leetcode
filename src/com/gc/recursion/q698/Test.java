package com.gc.recursion.q698;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 17:47
 */
public class Test {
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum % k != 0) return false;
        Arrays.sort(nums);
        return helper(nums, new int[k], sum / k, nums.length - 1);
    }

    boolean helper(int[] nums, int[] buckets, int target, int idx) {
        if (idx == -1) return true;
        if (nums[idx] > target) return false;
        for (int i = 0; i < buckets.length; i++) {
            if (buckets[i] + nums[idx] > target) continue;
            buckets[i] += nums[idx];
            if(helper(nums, buckets, target, idx - 1)) return true;
            buckets[i] -= nums[idx];
            if (buckets[i] == 0) return false;
        }
        return false;
    }

}