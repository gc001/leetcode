package com.gc.recursion.q698;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 划分为k个相等的子集
 * @author: gc
 * @create: 2022-05-08 15:21
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().canPartitionKSubsets(
                new int[]{3522, 181, 521, 515, 304, 123, 2512, 312, 922, 407, 146, 1932, 4037, 2646, 3871, 269}, 5));
    }

    public boolean canPartitionKSubsets(int[] nums, int k) {
        if (k == 1) return true;
        if (k > nums.length) return false;
        Arrays.sort(nums);
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum % k != 0) return false;
        int target = sum / k;
        int[] buckets = new int[k];
        return helper(nums, buckets, nums.length - 1, target);
    }

    boolean helper(int[] nums, int[] buckets, int idx, int target) {
        if (idx == -1) return true; // 既然没有 不合适的值和桶，那么肯定是已经被瓜分完毕了
        if (nums[idx] > target) return false;
        for (int i = buckets.length - 1; i >= 0; i--) {
            // 该桶放不下，尝试放下一个桶
            if (nums[idx] + buckets[i] > target) continue;
            // 放入该桶
            buckets[i] += nums[idx];
            // 处理下一个小球
            if (helper(nums, buckets, idx - 1, target)) return true;
            // 将小球拿出该桶
            buckets[i] -= nums[idx];
            // 如果处理了一圈都不行
            if (buckets[i] == 0) return false;
        }
        return false;
    }

}