package com.gc.recursion.offer.q13;

/**
 * @program: leetcode
 * @description: 机器人的运动范围
 * @author: gc
 * @create: 2022-05-05 23:36
 */
public class Solution {

    public int movingCount(int m, int n, int k) {
        return helper(m, n, k, 0, 0, new boolean[m][n]);
    }

    int helper(int m, int n, int k, int x, int y, boolean[][] visited) {
        if (x >= m || x < 0 || y >= n || y < 0 || visited[x][y] || !check(x, y, k)) return 0;
        visited[x][y] = true;
        return 1 + helper(m, n, k, x + 1, y, visited)
                + helper(m, n, k, x - 1, y, visited)
                + helper(m, n, k, x, y + 1, visited)
                + helper(m, n, k, x, y - 1, visited);
    }

    boolean check(int i, int j, int k) {
        return sum(i) + sum(j) <= k;
    }

    int sum(int x) {
        int res = 0;
        while (x != 0) {
            res += x % 10;
            x /= 10;
        }
        return res;
    }
}