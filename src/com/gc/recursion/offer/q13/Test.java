package com.gc.recursion.offer.q13;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 12:08
 */
public class Test {
    public int movingCount(int m, int n, int k) {
        return helper(m, n, 0, 0, k, new boolean[m][n]);
    }

    int helper(int m, int n, int i, int j, int k, boolean[][] v) {
        if (i >= m || i < 0 || j >= n || j < 0 || v[i][j] || !check(i, j, k)) return 0;
        v[i][j] = true;
        return 1 + helper(m, n, i - 1, j, k, v)
                + helper(m, n, i + 1, j, k, v)
                + helper(m, n, i, j - 1, k, v)
                + helper(m, n, i, j + 1, k, v);
    }

    boolean check(int i, int j, int k) {
        return sum(i) + sum(j) <= k;
    }

    int sum(int x) {
        int res = 0;
        while (x != 0) {
            res += x % 10;
            x /= 10;
        }
        return res;
    }
}