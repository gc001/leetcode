package com.gc.recursion.q52;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 11:31
 */
public class Test {

    int res = 0;
    public int totalNQueens(int n) {
        helper(n,0,new int[n]);
        return res;
    }

    void helper(int n, int k, int[] arr) {
        if (k == n) {
            res++;
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            arr[k] = i;
            if (check(k, arr)) {
                helper(n,k+1,arr);
            }
        }
    }

    boolean check(int k, int[] arr) {
        for (int j = 0; j < k; j++) {
            if (arr[j] == arr[k] || Math.abs(arr[k] - arr[j]) == k - j) return false;
        }
        return true;
    }
}