package com.gc.recursion.q52;

/**
 * @program: leetcode
 * @description: N皇后 II
 * @author: gc
 * @create: 2022-04-03 15:56
 */
public class Solution {
    int res = 0;

    public int totalNQueens(int n) {
        helper(n, 0, new int[n]);
        return res;
    }

    /**
     * @param arr i: 放在第i行；arr[i]：放在第arr[i]列
     * @param n   共n个
     * @param k   第k个
     */
    void helper(int n, int k, int[] arr) {
        if (k == n) {
            res++;
            return;
        }
        for (int i = 0; i < n; i++) {
            // 第k个(第k行)放在第i列
            arr[k] = i;
            if (check(arr, k)) {
                helper(n, k + 1, arr);
            }
        }
    }

    /** 校验放置的第k个是否正确 */
    boolean check(int[] arr, int k) {
        for (int i = 0; i < k; i++) {
            // 如果第k个和之前的放在同一列
            if (arr[i] == arr[k]
                    // 如果第k个和之前的放在同一斜线
                    || Math.abs(arr[k] - arr[i]) == k - i) return false;
        }
        return true;
    }
}














