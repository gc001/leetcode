package com.gc.recursion.q50;

/**
 * @program: leetcode
 * @description: Pow(x, n)
 * @author: gc
 * @create: 2022-03-28 21:36
 */
public class Solution {
    public double myPow(double x, int n) {
        if (x == 0) return 0;
        if (x == 1) return 1;
        long m = n;
        if (n < 0) {
            m *= -1;
        }
        double res = helper(x, m);
        if (n < 0) res = 1 / res;
        return res;
    }

    double helper(double x, long n) {
        if (n == 0) return 1;
        if (n == 1) return x;
        double d = helper(x, n / 2);
        if (n % 2 == 1) {
            d *= d * x;
        } else {
            d *= d;
        }
        return d;
    }

}