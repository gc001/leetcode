package com.gc.recursion.q50;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 11:25
 */
public class Test {
    public double myPow(double x, int n) {
        if (x == 0) return 0;
        boolean f = true;
        long m = n;
        if (m < 0) {
            f = false;
            m = -m;
        }
        double y = helper(x, m);
        if (!f) y = 1 / y;
        return y;
    }

    double helper(double x, long n) {
        if (n == 0) return 1;
        if (n == 1) return x;
        double half = helper(x, n / 2);
        double y = half * half;
        if (n % 2 == 1) y *= x;
        return y;
    }
}