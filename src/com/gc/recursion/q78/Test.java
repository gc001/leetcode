package com.gc.recursion.q78;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 14:28
 */
public class Test {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        helper(nums, 0, list, new ArrayList<>());
        return list;
    }

    void helper(int[] nums, int idx, List<List<Integer>> list, List<Integer> sub) {
        list.add(new ArrayList<>(sub));
        for (int i = idx; i < nums.length; i++) {
            sub.add(nums[i]);
            helper(nums, i + 1, list, sub);
            sub.remove(sub.size() - 1);
        }
    }
}