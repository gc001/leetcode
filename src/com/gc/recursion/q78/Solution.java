package com.gc.recursion.q78;

import java.util.ArrayList;
import java.util.List;

/**
 *  子集
 */
public class Solution {
    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> subsets(int[] nums) {
        helper(0, new ArrayList<>(), nums);
        return list;
    }

    void helper(int idx, List<Integer> sub, int[] nums) {
        list.add(new ArrayList<>(sub));
        for (int i = idx; i < nums.length; i++) {
            sub.add(nums[i]);
            helper(i + 1, sub, nums);
            sub.remove(sub.size() - 1);
        }
    }
}
