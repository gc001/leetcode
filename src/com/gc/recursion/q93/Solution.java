package com.gc.recursion.q93;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 复原 IP 地址
 * @author: gc
 * @create: 2022-04-04 18:44
 */
public class Solution {


    public List<String> restoreIpAddresses(String s) {
        List<String> list = new ArrayList<>();
        if (s == null || s.length() > 12 || s.length() < 4) return list;
        helper(s, "", 0, 0, list);
        return list;
    }

    void helper(String s, String sub, int idx, int cnt, List<String> list) {
        if (cnt > 4) return;
        if (idx == s.length()) {
            if (cnt == 4) {
                list.add(sub.substring(1));
            }
            return;
        }
        String part = "";
        for (int i = idx; i < idx + 3 && i < s.length(); i++) {
            part += s.charAt(i);
            if (Integer.parseInt(part) > 255) break;
            if (part.charAt(0) == '0' && part.length() > 1) break;
            helper(s, sub + "." + part, i + 1, cnt + 1, list);
        }
    }
}