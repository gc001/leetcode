package com.gc.recursion.q93;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 14:40
 */
public class Test {
    public List<String> restoreIpAddresses(String s) {
        List<String> list = new ArrayList<>();
        if (s == null || s.length() <= 3) return list;
        helper(s, 0, 0, "", list);
        return list;
    }

    void helper(String s, int idx, int cnt, String sub, List<String> list) {
        if (cnt == 4 && idx == s.length()) {
            list.add(sub.substring(0, sub.length() - 1));
            return;
        }
        if (cnt > 4 || idx == s.length()) return;
        for (int i = idx; i < idx + 3 && i < s.length(); i++) {
            String t = s.substring(idx, i + 1);
            if (Integer.parseInt(t) > 255 || (t.length() >= 2 && Integer.parseInt(t) < 10)) return;
            helper(s, i + 1, cnt + 1, sub + t + ".", list);
        }
    }
}