package com.gc.recursion.q40;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: leetcode
 * @description: 组合总和 II
 * @author: gc
 * @create: 2022-04-02 20:58
 */
public class Solution {

    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        helper(candidates, target, 0, new ArrayList<>());
        return list;
    }

    void helper(int[] arr, int target, int idx, List<Integer> sub) {
        if (target == 0) {
            list.add(new ArrayList<>(sub));
        }
        if (idx >= arr.length || target < 0) return;
        for (int i = idx; i < arr.length; i++) {
            if (i > idx && arr[i] == arr[i - 1]) {
                continue;
            }
            sub.add(arr[i]);
            helper(arr, target - arr[i], i + 1, sub);
            sub.remove(sub.size() - 1);
        }
    }


}