package com.gc.recursion.q40;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> lists = new ArrayList<>();
        Arrays.sort(candidates);
        helper(candidates, 0, target, lists, new ArrayList<>());
        return lists;
    }

    void helper(int[] arr, int idx, int target, List<List<Integer>> lists, List<Integer> sub) {
        if (idx == arr.length || target < 0) return;
        if (target == 0) {
            lists.add(new ArrayList<>(sub));
            return;
        }
        for (int i = idx; i < arr.length; i++) {
            if (i > idx && arr[i] == arr[i - 1]) continue;
            sub.add(arr[i]);
            helper(arr, i + 1, target - arr[i], lists, sub);
            sub.remove(sub.size() - 1);
        }
    }
}
