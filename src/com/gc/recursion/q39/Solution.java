package com.gc.recursion.q39;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 组合总和
 * @author: gc
 * @create: 2022-04-02 20:58
 */
public class Solution {

    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        helper(candidates, target, 0, new ArrayList<>());
        return list;
    }

    void helper(int[] arr, int target, int idx, List<Integer> sub) {
        if (target == 0) {
            list.add(new ArrayList<>(sub));
        }
        if (idx >= arr.length || target < 0) return;
        // 依次从第idx个开始选择
        for (int i = idx; i < arr.length; i++) {
            sub.add(arr[i]);
            // 默认一直选择当前的值，直至不符合条件了（target < 0），回退选择下一个
            helper(arr, target - arr[i], i, sub);
            sub.remove(sub.size() - 1);
        }
    }


}