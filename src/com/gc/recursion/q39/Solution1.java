package com.gc.recursion.q39;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 组合总和
 * @author: gc
 * @create: 2022-04-02 20:58
 */
public class Solution1 {

    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        helper(candidates, target, 0, new ArrayList<>());
        return list;
    }

    void helper(int[] arr, int target, int idx, List<Integer> sub) {
        if (target == 0) {
            list.add(new ArrayList<>(sub));
            return;
        }
        if (idx >= arr.length) return;
        // 放弃
        helper(arr, target, idx + 1, sub);
        // 选择
        if (target - arr[idx] >= 0) {
            sub.add(arr[idx]);
            helper(arr, target - arr[idx], idx, sub);
            sub.remove(sub.size() - 1);
        }
    }


}