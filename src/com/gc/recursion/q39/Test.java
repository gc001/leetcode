package com.gc.recursion.q39;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> lists = new ArrayList<>();
        helper(candidates, 0, target, lists, new ArrayList<>());
        return lists;
    }

    void helper(int[] arr, int idx, int target, List<List<Integer>> lists, List<Integer> sub) {
        if (idx == arr.length || target < 0) return;
        if (target == 0) {
            lists.add(new ArrayList<>(sub));
            return;
        }
        for (int i = idx; i < arr.length; i++) {
            sub.add(arr[i]);
            helper(arr, i, target - arr[i], lists, sub);
            sub.remove(sub.size() - 1);
        }
    }
}
