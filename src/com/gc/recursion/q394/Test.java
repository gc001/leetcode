package com.gc.recursion.q394;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 15:02
 */
public class Test {
    public String decodeString(String s) {
        if (s == null || s.length() <= 2) return s;
        Deque<Integer> numStk = new LinkedList<>();
        Deque<String> strStk = new LinkedList<>();
        String cur = "";
        int cnt = 0;
        for (char ch : s.toCharArray()) {
            if (ch >= '0' && ch <= '9') {
                cnt = 10 * cnt + (ch - '0');
            } else if (ch == '[') {
                numStk.push(cnt);
                cnt = 0;
                strStk.push(cur);
                cur = "";
            } else if (ch == ']') {
                int times = numStk.pop();
                StringBuilder sb = new StringBuilder();
                sb.append(strStk.pop());
                for (int i = 0; i < times; i++) {
                    sb.append(cur);
                }
                cur = sb.toString();
            } else {
                cur += ch;
            }
        }
        return cur;
    }
}