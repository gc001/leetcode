package com.gc.recursion.q394;

import java.util.Stack;

/**
 * @program: leetcode
 * @description: 字符串解码
 * @author: gc
 * @create: 2022-03-28 22:06
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().decodeString("3[a]2[bc]"));
        System.out.println(new Solution().decodeString("3[ab2[cd]]abc"));
        System.out.println(new Solution().decodeString("abc3[cd]xyz"));

    }

    public String decodeString(String s) {
        Stack<String> strStk = new Stack<>();
        Stack<Integer> cntStk = new Stack<>();
        int cnt = 0;
        String res = "";
        for (char ch : s.toCharArray()) {
            if (ch >= '0' && ch <= '9') {
                cnt = 10 * cnt + ch - '0';
            } else if (ch == '[') {
                strStk.push(res);
                cntStk.push(cnt);
                res = "";
                cnt = 0;
            } else if (ch == ']') {
                int times = cntStk.pop();
                StringBuilder sb = new StringBuilder();
                sb.append(strStk.pop());
                for (int i = 0; i < times; i++) {
                    sb.append(res);
                }
                res = sb.toString();
            } else {
                res += ch;
            }

        }
        return res;
    }
}