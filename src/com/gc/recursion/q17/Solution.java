package com.gc.recursion.q17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 电话号码的字母组合
 */
public class Solution {
    List<String> list = new ArrayList<>();

    public List<String> letterCombinations(String digits) {
        if (null == digits || digits.length() == 0) return list;
        Map<Character, String> map = new HashMap<>();
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
        helper(0, digits, "", map);
        return list;
    }


    void helper(int idx, String digits, String sub, Map<Character, String> map) {
        if (idx == digits.length()) {
            list.add(sub);
            return;
        }
        char[] chs = map.get(digits.charAt(idx)).toCharArray();
        for (char ch : chs) {
            helper(idx + 1, digits, sub + ch, map);
        }
    }
}
