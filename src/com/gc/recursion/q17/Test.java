package com.gc.recursion.q17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 10:52
 */
public class Test {
    public List<String> letterCombinations(String digits) {
        if (digits == null || digits.length() == 0) {
            return new ArrayList<>();
        }
        Map<Character, String> map = new HashMap<>();
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
        List<String> list = new ArrayList<>();
        helper(list, digits, 0, "", map);
        return list;
    }

    void helper(List<String> list, String digits, int idx, String sub, Map<Character, String> map) {
        if (idx == digits.length()) {
            list.add(sub);
            return;
        }
        String s = map.get(digits.charAt(idx));
        for (int i = 0; i < s.length(); i++) {
            helper(list, digits, idx + 1, sub + s.charAt(i), map);
        }
    }
}