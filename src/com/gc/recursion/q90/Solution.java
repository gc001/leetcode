package com.gc.recursion.q90;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *  子集 II
 */
public class Solution {
    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums);
        helper(nums,new ArrayList<>(),0);
        return list;
    }

    void helper(int[] nums, List<Integer> sub, int idx) {
        list.add(new ArrayList<>(sub));
        for (int i = idx; i < nums.length; i++) {
            if (i > idx && nums[i] == nums[i - 1]) {
                continue;
            }
            sub.add(nums[i]);
            helper(nums, sub, i + 1);
            sub.remove(sub.size() - 1);
        }
    }
}
