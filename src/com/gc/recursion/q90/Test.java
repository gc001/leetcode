package com.gc.recursion.q90;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 14:37
 */
public class Test {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        helper(nums, 0, list, new ArrayList<>());
        return list;
    }

    void helper(int[] nums, int idx, List<List<Integer>> list, List<Integer> sub) {
        list.add(new ArrayList<>(sub));
        for (int i = idx; i < nums.length; i++) {
            if (i > idx && nums[i - 1] == nums[i]) continue;
            sub.add(nums[i]);
            helper(nums, i + 1, list, sub);
            sub.remove(sub.size() - 1);

        }
    }
}