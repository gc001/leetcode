package com.gc.recursion.q231;

/**
 * @program: leetcode
 * @description: 2 的幂
 * @author: gc
 * @create: 2022-03-28 21:51
 */
public class Solution {
    public boolean isPowerOfTwo(int n) {
        if (n <= 0) return false;
        return (n & (n - 1)) == 0;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            if (new Solution().isPowerOfTwo(i)) {
                System.out.println(i);
            }

        }

    }
}