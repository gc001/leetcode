package com.gc.recursion.q22;

import java.util.ArrayList;
import java.util.List;

/**
 *  括号生成
 */
public class Solution {

    List<String> res = new ArrayList<>();

    public List<String> generateParenthesis(int n) {
        if (n == 0) return res;
        helper(n, n, "");
        return res;
    }

    void helper(int left, int right, String sub) {
        if (left < 0 || right < 0 || left > right) return;
        if (left == right && left == 0) {
            res.add(sub);
            return;
        }
        helper(left - 1, right, sub + "(");
        helper(left, right - 1, sub + ")");
    }
}
