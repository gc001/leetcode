package com.gc.recursion.q22;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 11:00
 */
public class Test {
    public List<String> generateParenthesis(int n) {
        List<String> list = new ArrayList<>();
        helper(n, 0, 0, "", list);
        return list;
    }

    void helper(int n, int left, int right, String sub, List<String> list) {
        if (left == right && right == n) {
            list.add(sub);
            return;
        }
        if (left < right || left > n || right > n) return;
        helper(n, left + 1, right, sub + '(', list);
        helper(n, left, right + 1, sub + ')', list);
    }
}