package com.gc.recursion.q46;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 11:05
 */
public class Test {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        helper(nums,0,list);
        return list;
    }

    void helper(int[] nums, int idx, List<List<Integer>> list) {
        if(idx == nums.length){
            List<Integer> sub = new ArrayList<>();
            for (int num : nums) {
                sub.add(num);
            }
            list.add(new ArrayList<>(sub));
            return;
        }
        for (int i = idx; i < nums.length; i++) {
            swap(nums,i,idx);
            helper(nums,idx+1,list);
            swap(nums,i,idx);
        }

    }

    void swap(int[] arr,int i,int j){
        int t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }
}