package com.gc.recursion.q46;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: leetcode
 * @description: 全排列
 * @author: gc
 * @create: 2022-04-03 15:15
 */
public class Solution {

    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> permute(int[] nums) {
        helper(nums, 0);
        return list;
    }

    /**
     * 全排列就是交换，确定第idx个，然后idx个依次和idx后面的第i个交换，这个递归函数里面的就是idx+1而不是i+1了
     * 核心思想是交换，具体来说，对于一个长度为n的串，要得到其所有排列，我们可以这样做：
     * 1.把当前位上的元素依次与其后的所有元素进行交换
     * 2.对下一位做相同处理，直到当前位是最后一位为止，输出序列
     *
     * @param nums arr
     * @param idx  idx
     */
    void helper(int[] nums, int idx) {
        if (idx >= nums.length) {
            List<Integer> sub = new ArrayList<>();
            for (int num : nums) {
                sub.add(num);
            }
            list.add(sub);
            return;
        }
        for (int i = idx; i < nums.length; i++) {
            swap(i, idx, nums);
            helper(nums, idx + 1);
            swap(i, idx, nums);
        }
    }

    void swap(int i, int j, int[] nums) {
        int num = nums[i];
        nums[i] = nums[j];
        nums[j] = num;
    }
}