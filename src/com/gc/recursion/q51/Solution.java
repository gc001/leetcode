package com.gc.recursion.q51;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: N 皇后
 * @author: gc
 * @create: 2022-04-03 15:56
 */
public class Solution {

    List<List<String>> list = new ArrayList<>();
    List<String> sub = new ArrayList<>();

    public List<List<String>> solveNQueens(int n) {
        helper(n, 0, new int[n]);
        return list;
    }

    /**
     * @param arr i: 放在第i行；arr[i]：放在第arr[i]列
     * @param n   共n个
     * @param k   第k个
     */
    void helper(int n, int k, int[] arr) {
        if (k == n) {
            list.add(new ArrayList<>(sub));
            return;
        }
        for (int i = 0; i < n; i++) {
            // 第k个放在第i列
            arr[k] = i;
            if (check(arr, k)) {
                sub.add(putQueen(sub, i, n));
                helper(n, k + 1, arr);
                sub.remove(sub.size() - 1);
            }
        }
    }

    boolean check(int[] arr, int k) {
        for (int i = 0; i < k; i++) {
            if (arr[i] == arr[k]
                    || Math.abs(arr[k] - arr[i]) == k - i) return false;
        }
        return true;
    }

    String putQueen(List<String> sub, int idx, int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(i == idx ? "Q" : ".");
        }
        return sb.toString();
    }
}














