package com.gc.recursion.q51;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-28 12:20
 */
public class Test {

    public List<List<String>> solveNQueens(int n) {
        List<List<String>> list = new ArrayList<>();
        helper(n, 0, new int[n], list, new ArrayList<>());
        return list;
    }

    void helper(int n, int k, int[] arr, List<List<String>> list, List<String> sub) {
        if (k == n) {
            list.add(new ArrayList<>(sub));
            return;
        }
        for (int i = 0; i < n; i++) {
            arr[k] = i;
            if (check(k, arr)) {
                sub.add(putQueen(n, i));
                helper(n, k + 1, arr, list, sub);
                sub.remove(sub.size() - 1);
            }
        }
    }

    private String putQueen(int n, int k) {
        StringBuilder sb = new StringBuilder(n);
        for (int j = 0; j < n; j++) {
            sb.append(j == k ? 'Q' : '.');
        }
        return sb.toString();
    }

    boolean check(int k, int[] arr) {
        for (int j = 0; j < k; j++) {
            if (arr[j] == arr[k] || Math.abs(arr[k] - arr[j]) == k - j) return false;
        }
        return true;
    }

}