package com.gc.recursion.q306;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-04-04 19:28
 */
public class Solution1 {

    public static void main(String[] args) {
        Solution1 solution = new Solution1();
        System.out.println(solution.isAdditiveNumber("1991001991"));
    }

    public boolean isAdditiveNumber(String num) {
        if (num == null || num.length() < 3) return false;
        for (int i = 0; i < num.length() - 2; i++) {
            for (int j = i + 1; j < num.length() - 1; j++) {
                if (check(num.substring(0, i + 1), num.substring(i + 1, j + 1), num)) {
                    return true;
                }
            }
        }
        return false;
    }

    boolean check(String firstStr, String secondStr, String num) {
        if ((firstStr.startsWith("0") && firstStr.length() > 1)
                || (secondStr.startsWith("0") && secondStr.length() > 1))
            return false;

        String sum = bigNumAdd(firstStr, secondStr);
        StringBuilder current = new StringBuilder(firstStr + secondStr + sum);
        while (current.length() < num.length()) {
            firstStr = secondStr;
            secondStr = sum;
            sum = bigNumAdd(firstStr, secondStr);
            current.append(sum);
        }
        return num.equals(current.toString());
    }

    String bigNumAdd(String s1, String s2) {
        if (s1.length() == 0 || s2.length() == 0)
            return "0";
        int len1 = s1.length();
        int len2 = s2.length();
        int[] arr1 = new int[len1];
        int[] arr2 = new int[len2];
        int[] res = new int[1 + (len1 > len2 ? len1 : len2)];
        for (int i = 0; i < len1; i++)
            arr1[len1 - i - 1] = s1.charAt(i) - '0';
        for (int i = 0; i < len2; i++)
            arr2[len2 - i - 1] = s2.charAt(i) - '0';
        int i = 0;
        int j = 0;
        int k = 0;
        int temp = 0;
        while (i < len1 || j < len2) {
            if (i < len1)
                temp += arr1[i++];
            if (j < len2)
                temp += arr2[j++];
            res[k++] = temp % 10;
            temp /= 10;
        }
        if (temp > 0)
            res[k] = temp;
        StringBuffer sb = new StringBuffer();

        boolean f = true;
        for (int n : res)
            sb.append(n);
        sb.reverse();
        for (i = 0; i < sb.length() - 1; i++) {
            if (sb.charAt(i) != '0')
                break;
        }
        return sb.toString().substring(i);
    }
}