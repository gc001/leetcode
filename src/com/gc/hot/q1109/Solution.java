package com.gc.hot.q1109;

/**
 * @program: leetcode
 * @description: 航班预订统计
 * @author: gc
 * @create: 2022-05-21 16:23
 */
public class Solution {
    public int[] corpFlightBookings(int[][] bookings, int n) {
        int[] arr = new int[n];
        for (int[] booking : bookings) {
            arr[booking[0] - 1] += booking[2];
            if (booking[1] < arr.length) arr[booking[1]] -= booking[2];
        }
        for (int i = 1; i < arr.length; i++) {
            arr[i] += arr[i - 1];
        }
        return arr;
    }
}