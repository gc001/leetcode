package com.gc.hot.q136;

/**
 * @program: leetcode
 * @description: 只出现一次的数字
 * @author: gc
 * @create: 2022-05-21 11:30
 */
public class Solution {
    public int singleNumber(int[] nums) {
        int res = 0;
        for (int num : nums) {
            res ^= num;
        }
        return res;
    }
}