package com.gc.hot.q238;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 除自身以外数组的乘积
 * @author: gc
 * @create: 2022-06-04 11:32
 */
public class Solution {
    public int[] productExceptSelf(int[] nums) {
        int[] arr = new int[nums.length];
        Arrays.fill(arr, 1);
        for (int i = 1; i < nums.length; i++) {
            arr[i] = arr[i - 1] * nums[i - 1];
        }
        int right = nums[nums.length - 1];
        for (int i = nums.length - 2; i >= 0; i--) {
            arr[i] *= right;
            right *= nums[i];
        }
        return arr;
    }
}