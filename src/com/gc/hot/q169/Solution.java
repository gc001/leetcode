package com.gc.hot.q169;

/**
 * @program: leetcode
 * @description: 多数元素
 * @author: gc
 * @create: 2022-05-21 16:53
 */
public class Solution {
    public int majorityElement(int[] nums) {
        int res = nums[0];
        int cnt = 0;
        for (int num : nums) {
            if (cnt == 0) {
                res = num;
                cnt++;
            } else if (res == num) {
                cnt++;
            } else {
                cnt--;
            }
        }
        return res;
    }
}