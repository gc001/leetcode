package com.gc.hot.q1094;

/**
 * @program: leetcode
 * @description: 拼车
 * @author: gc
 * @create: 2022-05-21 16:16
 */
public class Solution {
    public boolean carPooling(int[][] trips, int capacity) {
        int[] arr = new int[1001];
        for (int i = 0; i < trips.length; i++) {
            int from = trips[i][1];
            int to = trips[i][2];
            int cnt = trips[i][0];
            arr[from] -= cnt;
            arr[to] += cnt;
        }
        for (int a : arr) {
            capacity += a;
            if (capacity < 0) return false;
        }
        return true;
    }
}