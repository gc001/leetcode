package com.gc.hot.q461;

/**
 * @program: leetcode
 * @description: 汉明距离
 * @author: gc
 * @create: 2022-06-04 11:43
 */
public class Solution {
    public int hammingDistance(int x, int y) {
        int z = x ^ y;
        int res = 0;
        while (z != 0) {
            z &= (z - 1);
            res++;
        }
        return res;
    }
}