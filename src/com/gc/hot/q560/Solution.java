package com.gc.hot.q560;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: leetcode
 * @description: 和为 K 的子数组
 * @author: gc
 * @create: 2022-05-21 15:58
 * 我们之前知道区间和的公式等于k = sum[j] - sum[i - 1],
 * 我们通过简单的移项可以得出这个公式sum[i - 1] = sum[j] - k。
 * 我们在遍历nums时，可以获得当前的前缀和，当前的前缀和减去k，
 * 可以得到我们需要找的另一个前缀和的大小，如果hash之中有记录，
 * 我们只需要获取hash中的记录，就可以知道有多少区间和等于k了。
 *
 * 作者：dyhtps
 * 链接：https://juejin.cn/post/6944913393627168798
 * 来源：稀土掘金
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().subarraySum(new int[]{1, 1, 1}, 2));
    }

    public int subarraySum(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        int res = 0;
        int sum = 0;
        for (int num : nums) {
            sum += num;
            res += map.getOrDefault(sum - k, 0);
            map.put(sum, 1 + map.getOrDefault(sum, 0));
        }
        return res;
    }
}