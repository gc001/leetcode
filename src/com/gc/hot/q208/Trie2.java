package com.gc.hot.q208;

/**
 * 前缀树
 */
public class Trie2 {

    Node header = new Node('-');

    public Trie2() {

    }

    public void insert(String word) {
        if (word == null || word.length() == 0) {
            return;
        }
        Node node = header;
        for (char c : word.toCharArray()) {
            Node node1 = node.findNode(c);
            if (node1 != null) {
                node = node1;
            } else {
                node = node.createNode(c);
            }
        }
        node.isEnd = true;
    }

    public boolean search(String word) {
        if (word == null || word.length() == 0) {
            return false;
        }
        Node node = header;
        for (char c : word.toCharArray()) {
            node = node.findNode(c);
            if (node == null) {
                return false;
            }
        }
        return node.isEnd;
    }

    public boolean startsWith(String prefix) {
        if (prefix == null || prefix.length() == 0) {
            return false;
        }
        Node node = header;
        for (char ch : prefix.toCharArray()) {
            node = node.findNode(ch);
            if (node == null) {
                return false;
            }
        }
        return true;
    }

    static class Node {
        char ch;
        Node[] arr;
        boolean isEnd;

        public Node(char v) {
            ch = v;
            arr = new Node[26];
            isEnd = false;
        }

        public Node findNode(char v) {
            return arr[v - 'a'];
        }

        public Node createNode(char v) {
            Node node = new Node(v);
            arr[v - 'a'] = node;
            return node;
        }
    }
}
