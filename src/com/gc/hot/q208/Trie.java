package com.gc.hot.q208;

/**
 * 前缀树
 */
public class Trie {

    Node header = new Node('-');

    public Trie() {

    }

    public void insert(String word) {
        if (word == null || word.length() == 0) {
            return;
        }
        Node node = header;
        for (char c : word.toCharArray()) {
            if (node.hasNode(c)) {
                node = node.findNode(c);
            } else {
                node = node.createNode(c);
            }
        }
        node.isEnd = true;
    }

    public boolean search(String word) {
        if (word == null || word.length() == 0) {
            return false;
        }
        Node node = header;
        for (char c : word.toCharArray()) {
            if (node.hasNode(c)) {
                node = node.findNode(c);
            } else {
                return false;
            }
        }
        return node.isEnd;
    }

    public boolean startsWith(String prefix) {
        if (prefix == null || prefix.length() == 0) {
            return false;
        }
        Node node = header;
        for (char ch : prefix.toCharArray()) {
            if (node.hasNode(ch)) {
                node = node.findNode(ch);
            } else {
                return false;
            }
        }
        return true;
    }

    static class Node {
        char ch;
        Node[] arr;
        boolean isEnd;

        public Node(char v) {
            ch = v;
            arr = new Node[26];
            isEnd = false;
        }

        public boolean hasNode(char v) {
            return arr[v - 'a'] != null;
        }

        public Node findNode(char v) {
            return arr[v - 'a'];
        }

        public Node createNode(char v) {
            Node node = new Node(v);
            arr[v - 'a'] = node;
            return node;
        }
    }
}
