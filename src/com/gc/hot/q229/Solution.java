package com.gc.hot.q229;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: leetcode
 * @description: 多数元素 II
 * @author: gc
 * @create: 2022-05-21 17:11
 */
public class Solution {
    public List<Integer> majorityElement(int[] nums) {
        int n1 = -1;
        int n2 = 1;
        int c1 = 0;
        int c2 = 0;
        for (int num : nums) {
            // 如果先判断c1==0&c2==0会导致n1 n2是一个数，比如 1 2 3 3 3
            if (n1 == num) ++c1;
            else if (n2 == num) ++c2;
            else if (c1 == 0) {
                n1 = num;
                ++c1;
            } else if (c2 == 0) {
                n2 = num;
                ++c2;
            } else {
                c1--;
                c2--;
            }
        }
        c1 = 0;
        c2 = 0;
        for (int num : nums) {
            if (num == n1) ++c1;
            else if (num == n2) ++c2;
        }
        ArrayList<Integer> list = new ArrayList<>();
        if (c1 > nums.length / 3) list.add(n1);
        if (c2 > nums.length / 3) list.add(n2);
        return list;
    }
}