package com.gc.hot.q139;

import java.util.List;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 16:23
 */
public class Test {
    public boolean wordBreak(String s, List<String> wordDict) {
        boolean[] dp = new boolean[s.length() + 1];
        dp[0] = true;
        for (int i = 1; i <= s.length(); i++) {
            for (String word : wordDict) {
                if (i >= word.length() && word.equals(s.substring(i - word.length(), i)) && dp[i - word.length()]) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[s.length()];
    }
}