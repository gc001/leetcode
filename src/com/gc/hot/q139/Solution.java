package com.gc.hot.q139;

import java.util.List;

/**
 * @program: leetcode
 * @description: 单词拆分
 * @author: gc
 * @create: 2022-05-21 15:25
 */
public class Solution {
    public boolean wordBreak(String s, List<String> list) {
        if (list.size() == 0) return false;
        boolean[] dp = new boolean[s.length() + 1];
        dp[0] = true;
        for (int i = 1; i <= s.length(); i++) {
            for (String word : list) {
                int len = word.length();
                if (i >= len && s.substring(i - len, i).equals(word) && dp[i - len]) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[s.length()];
    }
}