package com.gc.hot.q79;

public class Solution1 {

    public boolean exist(char[][] board, String word) {
        int m = board.length;
        int n = board[0].length;
        if (m * n < word.length()) return false;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (helper(m, i, n, j, board, word)) return true;
            }
        }
        return false;
    }

    boolean helper(int m, int i, int n, int j, char[][] board, String word) {
        if (word.length() == 0) return true;
        if (i < 0 || i >= m || j < 0 || j >= n || word.charAt(0) != board[i][j]) return false;
        board[i][j] += 256;
        boolean res = helper(m, i - 1, n, j, board, word.substring(1))
                || helper(m, i + 1, n, j, board, word.substring(1))
                || helper(m, i, n, j - 1, board, word.substring(1))
                || helper(m, i, n, j + 1, board, word.substring(1));
        board[i][j] -= 256;
        return res;
    }
}