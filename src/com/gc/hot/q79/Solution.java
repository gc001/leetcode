package com.gc.hot.q79;

/**
 * @program: leetcode
 * @description: 单词搜索
 * @author: gc
 * @create: 2022-06-04 11:01
 */
public class Solution {

    public boolean exist(char[][] board, String word) {
        int m = board.length;
        int n = board[0].length;
        if (m * n < word.length()) return false;
        boolean[][] v = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (helper(m, i, n, j, v, board, 0, word)) return true;
            }
        }
        return false;
    }

    boolean helper(int m, int i, int n, int j, boolean[][] v, char[][] board, int idx, String word) {
        if (idx == word.length()) return true;
        if (i < 0 || i >= m || j < 0 || j >= n || v[i][j] || word.charAt(idx) != board[i][j]) return false;
        v[i][j] = true;
        boolean res = helper(m, i - 1, n, j, v, board, idx + 1, word)
                || helper(m, i + 1, n, j, v, board, idx + 1, word)
                || helper(m, i, n, j - 1, v, board, idx + 1, word)
                || helper(m, i, n, j + 1, v, board, idx + 1, word);
        v[i][j] = false;
        return res;
    }
}