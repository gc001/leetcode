package com.gc.hot.q347;

import java.util.*;

/**
 * @program: leetcode
 * @description: 前 K 个高频元素
 * @author: gc
 * @create: 2022-05-21 11:35
 */
public class Solution {
    public int[] topKFrequent(int[] nums, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        Queue<Integer> q = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return map.get(o1) - map.get(o2);
            }
        });
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (q.size() < k) {
                q.offer(e.getKey());
            } else {
                if (map.get(q.peek()) < e.getValue()) {
                    q.poll();
                    q.offer(e.getKey());
                }
            }
        }
        int[] arr = new int[k];
        int i = 0;
        while (!q.isEmpty()) {
            arr[i++] = q.poll();
        }
        return arr;
    }
}