package com.gc.hot.q621;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 任务调度器
 * @author: gc
 * @create: 2022-06-07 21:46
 */
public class Solution {
    /**
     * 解题思路：
     * 1、将任务按类型分组，正好A-Z用一个int[26]保存任务类型个数
     * 2、对数组进行排序，优先排列个数（count）最大的任务，
     * 如题得到的时间至少为 retCount =（count-1）* (n+1) + 1 ==> A->X->X->A->X->X->A(X为其他任务或者待命)
     * 3、再排序下一个任务，如果下一个任务B个数和最大任务数一致，
     * 则retCount++ ==> A->B->X->A->B->X->A->B
     * 4、如果空位都插满之后还有任务，那就随便在这些间隔里面插入就可以，因为间隔长度肯定会大于n，在这种情况下就是任务的总数是最小所需时间
     */
    public int leastInterval(char[] tasks, int n) {
        int[] arr = new int[26];
        for (char c : tasks) {
            arr[c - 'A']++;
        }
        Arrays.sort(arr);
        int max = arr[arr.length - 1];
        // 一共有几个最大值
        int maxCnt = 1;
        for (int i = arr.length - 2; i >= 0; i--) {
            if (arr[i] == max) maxCnt++;
            else break;
        }
        // 中间有几个空格
        int blank = (max - 1) * n - (maxCnt - 1);
        // 除掉第一个最大的之外剩余几个
        int remain = tasks.length - max * maxCnt;
        if (blank >= remain) {
            // 如果只有一个最大值
            if (maxCnt == 1) return blank + max;
                // 空格必然盛不下，将同样最大的往后排即可
            else return blank + max + maxCnt - 1;
        }
        // 如果空位都插满之后还有任务，那就随便在这些间隔里面插入就可以，因为间隔长度肯定会大于n，在这种情况下就是任务的总数是最小所需时间
        return tasks.length;
    }

    public static void main(String[] args) {
        System.out.println(new Solution().leastInterval(
                new char[]{'A', 'A', 'A', 'A', 'B', 'B', 'B', 'B', 'C', 'C', 'C', 'C', 'D', 'D', 'D', 'D', 'E', 'F'}, 4));
    }
}