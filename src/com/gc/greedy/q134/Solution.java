package com.gc.greedy.q134;

/**
 * @program: leetcode
 * @description: 加油站
 * @author: gc
 * @create: 2022-04-28 21:44
 */
public class Solution {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int min = 0;
        int n = gas.length;
        int remain = 0;
        int res = -1;
        for (int i = 0; i < n; i++) {
            remain += gas[i] - cost[i];
            if (min > remain) {
                min = remain;
                res = i;
            }
        }
        if (remain < 0) return -1;
        return res + 1 == n ? 0 : res + 1;
    }
}