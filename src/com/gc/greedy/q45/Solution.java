package com.gc.greedy.q45;

/**
 * @program: leetcode
 * @description: 跳跃游戏 II
 * @author: gc
 * @create: 2022-04-26 21:56
 */
public class Solution {
    public int jump(int[] nums) {
        if (nums.length <= 1) return 0;
        int max = 0;
        int idx = 0;
        int cnt = 0;
        while (idx < nums.length) {
            cnt++;
            if (nums[idx] + idx >= nums.length - 1) return cnt;
            int end = Math.min(nums.length - 1, nums[idx] + idx);
            // 记录下次应该从哪里起跳
            for (int i = idx + 1; i <= end; i++) {
                if (max < nums[i] + i) {
                    max = nums[i] + i;
                    idx = i;
                }
            }
        }
        return cnt;
    }
}