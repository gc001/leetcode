package com.gc.greedy.q45;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-31 16:50
 */
public class Test {
    public int jump(int[] nums) {
        if (nums.length <= 1) return 0;
        int idx = 0;
        int cnt = 0;
        int max = 0;
        while (idx < nums.length) {
            cnt++;
            if (nums[idx] + idx >= nums.length - 1) return cnt;
            int end = Math.min(nums.length - 1, nums[idx] + idx);
            for (int i = idx + 1; i <= end; i++) {
                if (max < nums[i] + i) {
                    max = nums[i] + i;
                    idx = i;
                }
            }
        }
        return cnt;
    }
}