package com.gc.greedy.q122;

/**
 * @program: leetcode
 * @description: 买卖股票的最佳时机 II
 * @author: gc
 * @create: 2022-04-28 21:31
 */
public class Solution {
    public int maxProfit(int[] prices) {
        if (prices.length <= 1) return 0;
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            res += Math.max(0, prices[i] - prices[i - 1]);
        }
        return res;
    }
}