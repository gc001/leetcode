package com.gc.greedy.q135;

/**
 * @program: leetcode
 * @description: 分发糖果
 * @author: gc
 * @create: 2022-04-28 21:11
 */
public class Solution {
    public int candy(int[] ratings) {
        if (ratings.length <= 1) return ratings.length;
        int[] left = new int[ratings.length];
        int[] right = new int[ratings.length];
        for (int i = 1; i < ratings.length; i++) {
            if (ratings[i] > ratings[i - 1]) {
                left[i] = left[i - 1] + 1;
            }
        }
        for (int i = ratings.length - 2; i >= 0; i--) {
            if (ratings[i] > ratings[i + 1]) {
                right[i] = right[i + 1] + 1;
            }
        }
        int res = ratings.length;
        for (int i = 0; i < ratings.length; i++) {
            res += Math.max(left[i], right[i]);
        }
        return res;
    }
}