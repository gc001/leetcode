package com.gc.greedy.q55;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-04-26 21:52
 */
public class Solution1 {
    public boolean canJump(int[] nums) {
        if (nums.length == 1) return true;
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (max < i) return false;
            max = Math.max(max, nums[i] + i);
            if (max >= nums.length - 1) return true;
        }
        return false;
    }
}