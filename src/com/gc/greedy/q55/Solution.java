package com.gc.greedy.q55;

/**
 * @program: leetcode
 * @description: 跳跃游戏
 * @author: gc
 * @create: 2022-04-25 20:48
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().canJump(new int[]{3,2,1,0,4}));
    }

    public boolean canJump(int[] nums) {
        if (nums.length <= 1) return true;
        int idx = 0;
        int max = nums[0];
        while (idx < nums.length) {
            // 到底终点
            if (nums[idx] + idx >= nums.length - 1) return true;
            int t = idx;
            // 寻找下一个能到达最大位置的点
            for (int step = 1; step <= nums[idx]; step++) {
                int nextIdx = idx + step;
                if (max < nums[nextIdx] + nextIdx) {
                    max = nums[nextIdx] + nextIdx;
                    t = nextIdx;
                }
            }
            // 如无更新证明无法前进
            if (t == idx) return false;
            // 更新idx
            idx = t;
        }
        return false;
    }
}