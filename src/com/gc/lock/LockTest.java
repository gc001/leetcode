package com.gc.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-15 18:00
 */
public class LockTest {
    public static void main(String[] args) throws InterruptedException {
        Lock lock = new ReentrantLock();
        Thread thread1 = new Thread(() -> {
            try {
                lock.lock();
                System.out.println("1111");
                Thread.sleep(10000);
                System.out.println("2222");
            } catch (Exception e) {
            } finally {
                lock.unlock();
            }
        });

        Thread thread2 = new Thread(() -> {
            try {
                lock.lockInterruptibly();
                System.out.println("3333");
            } catch (Exception e) {
                if (e instanceof InterruptedException)
                    System.out.println("我被中断了");
            } finally {
                System.out.println("4444");
                lock.unlock();
            }
        });

        thread1.start();
        Thread.sleep(5000);
        thread2.start();
         thread2.interrupt();
    }
}