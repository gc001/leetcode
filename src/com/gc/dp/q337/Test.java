package com.gc.dp.q337;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 15:23
 */
public class Test {
    public int rob(TreeNode root) {
        int[] helper = helper(root);
        return Math.max(helper[0], helper[1]);
    }

    int[] helper(TreeNode root) {
        // arr[0] 偷自身； arr[1] 不偷自身
        int[] arr = new int[2];
        if (root == null) return arr;
        int[] left = helper(root.left);
        int[] right = helper(root.right);
        // 偷自身
        arr[0] = root.val + left[1] + right[1];
        // 不偷自身
        arr[1] = Math.max(left[0], left[1]) + Math.max(right[0], right[1]);
        return arr;
    }

}