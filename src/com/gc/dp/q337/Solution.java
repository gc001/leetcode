package com.gc.dp.q337;

import com.gc.common.TreeNode;

/**
 * @program: leetcode
 * @description: 打家劫舍 III
 * @author: gc
 * @create: 2022-05-03 14:14
 */
public class Solution {
    public int rob(TreeNode root) {
        if (root == null) return 0;
        int[] dp = helper(root);
        return Math.max(dp[0], dp[1]);
    }

    /**
     * @param root root
     * @return int[] index  0: 偷， 1: 不偷
     */
    int[] helper(TreeNode root) {
        if (root == null) return new int[2];
        int[] left = helper(root.left);
        int[] right = helper(root.right);
        // 偷root
        int v1 = root.val + left[1] + right[1];
        // 不偷root,则左右偷不偷都可
        int v2 = Math.max(left[0], left[1]) + Math.max(right[0], right[1]);
        return new int[]{v1, v2};
    }
}