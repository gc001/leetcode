package com.gc.dp.q300;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 最长递增子序列
 * @author: gc
 * @create: 2022-05-03 09:34
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(new Solution().lengthOfLIS(new int[]{5, 6, 7, 1, 2, 3, 4}));
    }

    public int lengthOfLIS(int[] nums) {
        if (nums.length <= 1) return nums.length;
        int[] dp = new int[nums.length];
        int res = 0;
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) dp[i] = Math.max(dp[i], 1 + dp[j]);
                res = Math.max(res, dp[i]);
            }
        }
        return res+1;
    }
}