package com.gc.dp.q309;

/**
 * @program: leetcode
 * @description: 最佳买卖股票时机含冷冻期
 * @author: gc
 * @create: 2022-05-08 15:04
 */
public class Solution {
    public int maxProfit(int[] prices) {
        if (prices.length <= 1) return 0;
        int[] sell = new int[prices.length];
        int[] buy = new int[prices.length];
        int[] freeze = new int[prices.length];
        buy[0] = -prices[0];
        for (int i = 1; i < prices.length; i++) {
            sell[i] = Math.max(sell[i - 1], buy[i - 1] + prices[i]);
            buy[i] = Math.max(buy[i - 1], freeze[i - 1] - prices[i]);
            // 冷冻期，不能由buy那边过来
            freeze[i] = Math.max(freeze[i - 1], sell[i - 1]);
        }
        return Math.max(sell[prices.length - 1], Math.max(buy[prices.length - 1], freeze[prices.length - 1]));
    }

}