package com.gc.dp.q309;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 16:08
 */
public class Test {
    public int maxProfit(int[] prices) {
        int n = prices.length;
        if (n <= 1) return 0;
        int[] buy = new int[n];
        int[] sell = new int[n];
        int[] freeze = new int[n];
        buy[0] = -prices[0];
        for (int i = 1; i < n; i++) {
            buy[i] = Math.max(buy[i - 1], freeze[i - 1] - prices[i]);
            sell[i] = Math.max(buy[i - 1] + prices[i], sell[i - 1]);
            freeze[i] = Math.max(freeze[i - 1], sell[i - 1]);
        }
        return Math.max(freeze[n - 1], sell[n - 1]);
    }
}