package com.gc.dp.q416;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 16:32
 */
public class Test {
    public boolean canPartition(int[] nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum % 2 != 0) return false;
        int target = sum / 2;
        int[] dp = new int[1 + target];
        for (int i = 0; i < nums.length; i++) {
            for (int j = target; j >= 1; j--) {
                if (j >= nums[i]) dp[j] += dp[j - nums[i]];
                if (j == target && dp[j] > 0) return true;
            }
        }
        return false;
    }
}