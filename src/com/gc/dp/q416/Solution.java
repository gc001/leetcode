package com.gc.dp.q416;

/**
 * @program: leetcode
 * @description: 分割等和子集
 * @author: gc
 * @create: 2022-05-04 13:27
 */
public class Solution {
    public boolean canPartition(int[] nums) {
        if (nums.length <= 1) return false;
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum % 2 != 0) return false;
        int amount = sum / 2;
        boolean[] dp = new boolean[amount + 1];
        dp[0] = true;
        for (int i = 0; i < nums.length; i++) {
            for (int j = amount; j >= 1; j--) {
                if (j >= nums[i]) dp[j] = dp[j] || dp[j - nums[i]];
                if (dp[amount]) return true;
            }
        }
        return false;
    }
}