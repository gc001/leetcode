package com.gc.dp.q343;

/**
 * @program: leetcode
 * @description: 整数拆分
 * @author: gc
 * @create: 2022-05-03 11:40
 */
public class Solution {
    public int integerBreak(int n) {
        if (n <= 2) return 1;
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j < i - 1; j++) {
                dp[i] = Math.max(dp[i], dp[j] * (i - j));
                // dp[...]可能是被拆分之后的乘积，即dp[j] * (i - j)至少三个数，因此需要考虑2个数的情况
                dp[i] = Math.max(dp[i], j * (i - j));
            }
        }
        return dp[n];
    }
}