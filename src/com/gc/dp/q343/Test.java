package com.gc.dp.q343;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 15:27
 */
public class Test {
    public int integerBreak(int n) {
        if (n <= 2) return 1;
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j < i; j++) {
                dp[i] = Math.max(dp[i], (i - j) * Math.max(dp[j], j));
            }
        }
        return dp[n];
    }
}