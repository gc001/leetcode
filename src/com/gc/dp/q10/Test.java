package com.gc.dp.q10;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 17:12
 */
public class Test {

    public static void main(String[] args) {
        new Test().isMatch("aa","a*");
    }

    public boolean isMatch(String s, String p) {
        boolean[][] dp = new boolean[s.length() + 1][p.length() + 1];
        dp[0][0] = true;
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 1; j <= p.length(); j++) {
                if (p.charAt(j - 1) != '*') {
                    if (check(s, i, p, j)) {
                        dp[i][j] = dp[i][j] || dp[i - 1][j - 1];
                    }
                } else {
                    dp[i][j] = dp[i][j - 2];
                    if (check(s, i, p, j - 1)) {
                        dp[i][j] = dp[i][j] || dp[i - 1][j];
                    }
                }
            }
        }
        return dp[s.length()][p.length()];
    }

    boolean check(String s, int i, String p, int j) {
        return i != 0 && (p.charAt(j - 1) == '.' || p.charAt(j - 1) == s.charAt(i - 1));
    }
}