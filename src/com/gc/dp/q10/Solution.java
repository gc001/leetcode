package com.gc.dp.q10;

/**
 * @program: leetcode
 * @description: 正则表达式匹配
 * @author: gc
 * @create: 2022-05-04 15:27
 * @refer https://leetcode.cn/problems/regular-expression-matching/solution/zheng-ze-biao-da-shi-pi-pei-by-leetcode-solution/
 * <p>
 * 以一个例子详解动态规划转移方程：
 * S = abbbbc
 * P = ab*d*c
 * 1. 当 i, j 指向的字符均为字母（或 '.' 可以看成一个特殊的字母）时，
 * 只需判断对应位置的字符即可，
 * 若相等，只需判断 i,j 之前的字符串是否匹配即可，转化为子问题 f[i-1][j-1].
 * 若不等，则当前的 i,j 肯定不能匹配，为 false.
 * <p>
 * f[i-1][j-1]   i
 * |        |
 * S [a  b  b  b  b][c]
 * <p>
 * P [a  b  *  d  *][c]
 * |
 * j
 * <p>
 * <p>
 * 2. 如果当前 j 指向的字符为 '*'，则不妨把类似 'a*', 'b*' 等的当成整体看待。
 * 看下面的例子
 * <p>
 * i
 * |
 * S  a  b [b] b  b  c
 * <p>
 * P  a [b  *] d  *  c
 * |
 * j
 * <p>
 * 注意到当 'b*' 匹配完 'b' 之后，它仍然可以继续发挥作用。
 * 因此可以只把 i 前移一位，而不丢弃 'b*', 转化为子问题 f[i-1][j]:
 * <p>
 * i
 * | <--
 * S  a [b] b  b  b  c
 * <p>
 * P  a [b  *] d  *  c
 * |
 * j
 * <p>
 * 另外，也可以选择让 'b*' 不再进行匹配，把 'b*' 丢弃。
 * 转化为子问题 f[i][j-2]:
 * <p>
 * i
 * |
 * S  a  b [b] b  b  c
 * <p>
 * P [a] b  *  d  *  c
 * |
 * j <--
 * <p>
 * 3. 冗余的状态转移不会影响答案，
 * 因为当 j 指向 'b*' 中的 'b' 时, 这个状态对于答案是没有用的,
 * 原因参见评论区 稳中求胜 的解释, 当 j 指向 '*' 时,
 * dp[i][j]只与dp[i][j-2]有关, 跳过了 dp[i][j-1].
 */
public class Solution {
    public boolean isMatch(String s, String p) {
        // dp[i][j] 代表s前i个字符与p前j个字符是否匹配
        boolean[][] dp = new boolean[s.length() + 1][p.length() + 1];
        dp[0][0] = true;
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 1; j <= p.length(); j++) {
                if (p.charAt(j - 1) == '*') {
                    // 分支1： 不计算该*与前一个字母
                    dp[i][j] = dp[i][j - 2];
                    // 分支2： 注意到当 'b*' 匹配完 'b' 之后，它仍然可以继续发挥作用。
                    // 因此可以只把 i 前移一位，而不丢弃 'b*', 转化为子问题 f[i-1][j]:(即p中该*的前一位与s的第i位相同，p的这个(b*)继续去匹配s的第i-1位)
                    if (check(s, p, i, j - 1)) {
                        dp[i][j] = dp[i][j] || dp[i-1][j];
                    }
                } else {
                    // s第i个字符与p第j个字符是否相同
                    if (check(s, p, i, j)) {
                        // 如果相同，则只需要看s的i-1个和p的j-1个
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                }
            }
        }
        return dp[s.length()][p.length()];
    }

    /* 这个check的使用条件是p.charAt(j) != '*' */
    boolean check(String s, String p, int i, int j) {
        // s的第0个不存在，j又不为0，则必不相同
        if (i == 0) return false;
        return p.charAt(j - 1) == '.' || p.charAt(j - 1) == s.charAt(i - 1);
    }
}