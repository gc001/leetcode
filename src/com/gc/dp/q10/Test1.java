package com.gc.dp.q10;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-06-11 21:18
 */
public class Test1 {
    public boolean isMatch(String s, String p) {
        boolean[][] dp = new boolean[s.length() + 1][p.length() + 1];
        dp[0][0] = true;
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 1; j <= p.length(); j++) {
                if (p.charAt(j - 1) == '*') {
                    dp[i][j] = dp[i][j] || dp[i][j - 2];
                    if (check(i - 1, j, s, p)) {
                        dp[i][j] = dp[i][j] || dp[i][j - 1];
                    }
                } else {
                    if (check(i, j, s, p)) dp[i][j] = true;
                }
            }
        }
        return dp[s.length()][p.length()];
    }

    boolean check(int i, int j, String s, String p) {
        if (i == 0) return false;
        return p.charAt(j - 1) == '.' || s.charAt(i - 1) == p.charAt(j - 1);
    }
}