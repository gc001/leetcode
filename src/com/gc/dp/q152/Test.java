package com.gc.dp.q152;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 14:02
 */
public class Test {
    public int maxProduct(int[] nums) {
        int max = nums[0];
        int min = nums[0];
        int res = nums[0];
        for (int i = 1; i < nums.length; i++) {
            int tMax = max, tMin = min;
            max = Math.max(nums[i], Math.max(tMax * nums[i], tMin * nums[i]));
            min = Math.min(nums[i], Math.min(tMax * nums[i], tMin * nums[i]));
            res = Math.max(res, max);
        }
        return res;
    }
}