package com.gc.dp.q152;

/**
 *  乘积最大子数组
 */
class Solution {
    public int maxProduct(int[] nums) {
        // max 以nums[i]结尾的最大值
        int max = nums[0], min = nums[0], res = nums[0];
        for (int i = 1; i < nums.length; i++) {
            int _min = min;
            int _max = max;
            min = Math.min(nums[i], Math.min(_min * nums[i], _max * nums[i]));
            max = Math.max(nums[i], Math.max(_min * nums[i], _max * nums[i]));
            res = Math.max(res, max);
        }
        return res;
    }
}