package com.gc.dp.q279;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 15:53
 */
public class Test {
    public int numSquares(int n) {
        int[] dp = new int[n + 1];
        Arrays.fill(dp, 1000);
        for (int i = 1; i * i <= n; i++) {
            dp[i * i] = 1;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j * j < i; j++) {
                dp[i] = Math.min(dp[i], 1 + dp[i - j * j]);
            }
        }
        return dp[n];
    }
}