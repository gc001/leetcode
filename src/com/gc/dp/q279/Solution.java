package com.gc.dp.q279;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description: 完全平方数
 * @author: gc
 * @create: 2022-05-03 12:00
 */
public class Solution {

    public int numSquares(int n) {
        int[] dp = new int[n + 1];
        Arrays.fill(dp, 1000);
        for (int i = 1; i * i <= n; i++) {
            dp[i * i] = 1;
        }
        for (int i = 2; i <= n; i++) {
            for (int j = 0; j * j < i; j++) {
                dp[i] = Math.min(dp[i], 1 + dp[i - j * j]);
            }
        }
        return dp[n];
    }

}