package com.gc.dp.q53;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 13:22
 */
public class Test {
    public int maxSubArray(int[] nums) {
        int sum = nums[0];
        int res = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (sum < 0) {
                sum = nums[i];
            } else {
                sum += nums[i];
            }
            res = Math.max(res, sum);
        }
        return res;
    }
}