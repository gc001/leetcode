package com.gc.dp.q53;

/**
 * @program: leetcode
 * @description: 最大子数组和
 * @author: gc
 * @create: 2022-05-02 23:35
 */
public class Solution {
    public int maxSubArray(int[] nums) {
        int res = nums[0];
        int sum = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (sum < 0) {
                sum = nums[i];
            } else {
                sum += nums[i];
            }
            res = Math.max(res, sum);
        }
        return res;
    }
}