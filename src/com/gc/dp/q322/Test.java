package com.gc.dp.q322;

import java.util.Arrays;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 15:34
 */
public class Test {
    public int coinChange(int[] coins, int amount) {
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, 1000000);
        dp[0] = 0;
        for (int i = 0; i < coins.length; i++) {
            for (int j = 0; j <= amount; j++) {
                if (j >= coins[i]) dp[j] = Math.min(dp[j], 1 + dp[j - coins[i]]);
            }
        }
        return dp[amount] == 1000000 ? -1 : dp[amount];
    }
}