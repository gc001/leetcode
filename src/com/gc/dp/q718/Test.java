package com.gc.dp.q718;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 12:47
 */
public class Test {
    public int findLength(int[] nums1, int[] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        int res = 0;
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 1; i <= nums1.length; i++) {
            for (int j = 1; j <= nums2.length; j++) {
                dp[i][j] = nums1[i - 1] == nums2[j - 1] ? Math.max(dp[i][j], dp[i - 1][j - 1] + 1) : 0;
                res = Math.max(res, dp[i][j]);
            }
        }
        return res;
    }
}