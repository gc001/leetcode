package com.gc.dp.q718;

/**
 * @program: leetcode
 * @description: 最长重复子数组[暴力解法]
 * @author: gc
 * @create: 2022-05-06 22:03
 */
public class Solution {

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 2, 1};
        int[] arr2 = {3, 2, 1, 4, 7};
        System.out.println(new Solution().findLength(arr1, arr2));
    }

    public int findLength(int[] nums1, int[] nums2) {
        int res = 0;
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                int t = 0;
                int _i = i;
                int _j = j;
                while (_i < nums1.length && _j < nums2.length) {
                    if (nums1[_i] == nums2[_j]) {
                        t++;
                        _i++;
                        _j++;
                    } else {
                        break;
                    }
                }
                res = Math.max(res, t);
            }
        }
        return res;
    }
}