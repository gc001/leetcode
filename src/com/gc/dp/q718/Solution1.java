package com.gc.dp.q718;

/**
 * @program: leetcode
 * @description: 最长重复子数组[dp]
 * @author: gc
 * @create: 2022-05-06 22:03
 */
public class Solution1 {

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 2, 1};
        int[] arr2 = {3, 2, 1, 4, 7};
        System.out.println(new Solution1().findLength(arr1, arr2));
    }

    public int findLength(int[] nums1, int[] nums2) {
        // 子数组是连续的，则dp[i][j]只能由dp[i-1][j-1]递推过来，不相同则为0
        // 若是子序列，则不是连续的， dp[i][j] = max(dp[i-1][j-1]+(A[i-1] == B[j-1]?1:0),dp[i-1][j],dp[i][j-1])
        int res = 0;
        int[][] dp = new int[nums1.length + 1][nums2.length + 1];
        for (int i = 1; i <= nums1.length; i++) {
            for (int j = 1; j <= nums2.length; j++) {
                if (nums1[i - 1] == nums2[j - 1]) {
                    dp[i][j] = 1 + dp[i - 1][j - 1];
                    res = Math.max(res, dp[i][j]);
                }
            }
        }
        return res;
    }
}