package com.gc.dp.q494;

/**
 * @program: leetcode
 * @description: 目标和
 * @author: gc
 * @create: 2022-05-05 23:45
 */
public class Solution {

    public int findTargetSumWays(int[] nums, int target) {
        int s = 0;
        for (int num : nums) {
            s += num;
        }

        if (s < target || target + s < 0 || (target + s) % 2 != 0) return 0;
        s = (target + s) / 2;
        int[][] dp = new int[nums.length + 1][s + 1];
        dp[0][0] = 1;
        for (int i = 1; i <= nums.length; i++) {
            for (int j = 0; j <= s; j++) {
                if (j >= nums[i - 1]) dp[i][j] = dp[i - 1][j] + dp[i - 1][j - nums[i - 1]];
                else dp[i][j] = dp[i - 1][j];
            }
        }
        return dp[nums.length][s];
    }
}