package com.gc.dp.q494;

/**
 * @program: leetcode
 * @description: 目标和
 * @author: gc
 * @create: 2022-05-05 23:45
 */
public class Solution1 {

    public int findTargetSumWays(int[] nums, int target) {
        int s = 0;
        for (int num : nums) {
            s += num;
        }
        if (s < target || s + target < 0 || (target + s) % 2 != 0) return 0;
        s = (target + s) / 2;
        int[] dp = new int[s + 1];
        dp[0] = 1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = s; j >= nums[i]; j--) {
                dp[j] += dp[j - nums[i]];
            }
        }
        return dp[s];
    }

}