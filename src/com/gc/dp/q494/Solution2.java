package com.gc.dp.q494;

/**
 * @program: leetcode
 * @description: 目标和
 * @author: gc
 * @create: 2022-05-05 23:45
 */
public class Solution2 {

    int res;

    public int findTargetSumWays(int[] nums, int target) {
        helper(nums, 0, target, 0);
        return res;
    }

    void helper(int[] nums, int sum, int target, int idx) {
        if (idx == nums.length) {
            if (sum == target) {
                res++;
            }
            return;
        }
        helper(nums, sum + nums[idx], target, idx + 1);
        helper(nums, sum - nums[idx], target, idx + 1);

    }


}