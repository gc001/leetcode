package com.gc.dp.q494;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 16:42
 */
public class Test {
    public int findTargetSumWays(int[] nums, int target) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum < target || target + sum < 0 || (sum + target) % 2 != 0) return 0;
        target = (sum + target) / 2;
        int[] dp = new int[1 + target];
        dp[0] = 1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = target; j >= 1; j--) {
                if (j >= nums[i]) dp[j] += dp[j - nums[i]];
            }
        }
        return dp[target];
    }
}