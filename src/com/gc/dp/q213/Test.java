package com.gc.dp.q213;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 15:18
 */
public class Test {
    public int rob(int[] nums) {
        if (nums.length == 1) return nums[0];
        if (nums.length == 2) return Math.max(nums[0], nums[1]);
        return Math.max(helper(nums, 0, nums.length - 2), helper(nums, 1, nums.length - 1));
    }

    int helper(int[] arr, int start, int end) {
        int[] dp = new int[arr.length];
        dp[start] = arr[start];
        dp[start + 1] = Math.max(arr[start], arr[start + 1]);
        for (int i = start + 2; i <= end; i++) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + arr[i]);
        }
        return dp[end];
    }
}