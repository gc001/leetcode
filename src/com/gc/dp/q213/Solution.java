package com.gc.dp.q213;

/**
 * @program: leetcode
 * @description: 打家劫舍 II
 * @author: gc
 * @create: 2022-05-03 14:05
 */
public class Solution {
    public int rob(int[] nums) {
        if (nums.length == 1) return nums[0];
        if (nums.length == 2) return Math.max(nums[0], nums[1]);
        int v1 = helper(nums, 0, nums.length - 2);
        int v2 = helper(nums, 1, nums.length - 1);
        return Math.max(v1, v2);
    }

    public int helper(int[] nums, int start, int end) {
        if (start == end) return nums[start];
        int[] dp = new int[nums.length];
        dp[start] = nums[start];
        dp[start + 1] = Math.max(nums[start], nums[start + 1]);
        for (int i = start + 2; i <= end; i++) {
            dp[i] = Math.max(nums[i] + dp[i - 2], dp[i - 1]);
        }
        return dp[end];
    }
}