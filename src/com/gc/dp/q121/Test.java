package com.gc.dp.q121;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 14:00
 */
public class Test {
    public int maxProfit(int[] prices) {
        int res = 0;
        int min = prices[0];
        for (int i = 1; i < prices.length; i++) {
            res = Math.max(res, prices[i] - min);
            min = Math.min(min, prices[i]);
        }
        return res;
    }
}