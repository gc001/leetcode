package com.gc.dp.q70;

/**
 * @program: leetcode
 * @description: 爬楼梯
 * @author: gc
 * @create: 2022-05-03 11:21
 */
public class Solution {
    public int climbStairs(int n) {
        if (n == 1) return 1;
        if (n == 2) return 2;
        int left = 1, right = 2;
        for (int i = 2; i <= n; i++) {
            int sum = left+right;
            left = right;
            right = sum;
        }
        return right;
    }
}