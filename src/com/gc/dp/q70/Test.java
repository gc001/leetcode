package com.gc.dp.q70;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 13:44
 */
public class Test {
    public int climbStairs(int n) {
        if (n <= 2) return n;
        int l = 1;
        int r = 2;
        for (int i = 3; i <= n; i++) {
            int t = r;
            r += l;
            l = t;
        }
        return r;
    }
}