package com.gc.dp.q62;

/**
 * @program: leetcode
 * @description: 不同路径
 * @author: gc
 * @create: 2022-05-05 23:40
 */
public class Solution1 {
    public int uniquePaths(int m, int n) {
        int[] dp = new int[n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) dp[j] = 1;
                else dp[j] = dp[j] + dp[j - 1];
            }
        }
        return dp[n - 1];
    }
}