package com.gc.dp.q96;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-05-30 15:42
 */
public class Test {
    public int numTrees(int n) {
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                dp[i] += dp[j] * dp[i - j - 1];
            }
        }
        return dp[n];
    }
}