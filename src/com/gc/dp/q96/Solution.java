package com.gc.dp.q96;

/**
 * @program: leetcode
 * @description: 不同的二叉搜索树
 * @author: gc
 * @create: 2022-05-07 21:54
 */
public class Solution {
    public int numTrees(int n) {
        // 以n为根节点： G(n) = G(0)*G(n-1)+G(1)*(n-2)+...+G(n-1)*G(0)
        int[] dp = new int[n + 1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                dp[i] += dp[j] * dp[i - j - 1];
            }
        }
        return dp[n];
    }
}