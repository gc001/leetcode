# 175
select p.firstName, p.lastName, a.city, a.state
from Person p
         left join Address a
                   on p.PersonId = a.PersonId;

# 180
## 1
select distinct a.num ConsecutiveNums
from Logs a,
     Logs b,
     Logs c
where a.id = b.id + 1
  and b.id = c.id + 1
  and a.num = b.num
  and a.num = c.num;
## 2
select distinct num ConsecutiveNums
from Logs
where (id + 1, num) in (select id, num from Logs)
  and (id + 2, num) in (select id, num from Logs);
## 3
select distinct num ConsecutiveNums
from Logs
where (id - 1, num) in (
    select a.id, a.num
    from Logs a,
         Logs b
    where a.id = b.id + 1
      and a.num = b.num
);

# 178 分数排名
select score, dense_rank() over (order by score desc) as 'rank'
from Scores

# 185 部门工资前三高的所有员工
## 1
## ee.salary >= e.salary 的数量不超过3个，即是此时e的salary就是前三名了
select d.Name   Department,
       e.name   Employee,
       e.Salary Salary
from Department d,
     Employee e
where e.DepartmentId = d.Id
  and (
          select COUNT(distinct salary)
          from Employee ee
          where ee.DepartmentId = e.DepartmentId
            AND ee.Salary >= e.Salary) <= 3
order by e.DepartmentId ASC, e.Salary Desc
## 2
select
    d.name Department,
    e1.name Employee,
    e1.salary
from employee e1, employee e2, Department d
where e1.departmentid = e2.departmentid and
        e1.salary <= e2.salary and
        e1.departmentid = d.id
group by e1.id, e1.departmentid
having count(distinct e2.salary) <= 3