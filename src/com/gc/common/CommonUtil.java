package com.gc.common;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @program: leetcode
 * @description:
 * @author: gc
 * @create: 2022-03-20 11:07
 */
public class CommonUtil {

    public static ListNode createListNode(Integer[] arr) {
        if (arr == null || arr.length == 0) return null;
        ListNode header = new ListNode(arr[0]);
        ListNode cur = header;
        for (int i = 1; i < arr.length; i++) {
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }
        return header;
    }

    public static TreeNode createTreeNode(Integer[] arr) {
        if (arr.length == 0) return null;
        return createTreeNode(arr, 0);
    }

    static TreeNode createTreeNode(Integer[] arr, int idx) {
        if (idx >= arr.length || arr[idx] == null) return null;
        TreeNode root = new TreeNode(arr[idx]);
        root.left = createTreeNode(arr, idx * 2 + 1);
        root.right = createTreeNode(arr, idx * 2 + 2);
        return root;
    }

    static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        while (q.size() > 0) {
            List<Integer> sub = new ArrayList<>();
            int cnt = q.size();
            for (int i = 0; i < cnt; i++) {
                TreeNode node = q.poll();
                sub.add(node.val);
                if (node.left != null) {
                    q.add(node.left);
                }
                if (node.right != null) {
                    q.add(node.right);
                }
            }
            list.add(sub);
        }
        return list;
    }
}